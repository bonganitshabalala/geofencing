package com.example.khumo.geofencing.Student;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.khumo.geofencing.BottomNavigationViewHelper;
import com.example.khumo.geofencing.Lecturer.ViewActivity;
import com.example.khumo.geofencing.MainActivity;
import com.example.khumo.geofencing.R;
import com.example.khumo.geofencing.adapter.SubjectAdapter;
import com.example.khumo.geofencing.pojo.Student;
import com.example.khumo.geofencing.pojo.Subject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class StudentHome extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    private String stuNumber;
    NavigationView navigationView;
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 10000;
    private static String urlcon = "http://petersdana98.000webhostapp.com/getAllSubjects.php";
    private static String urlConne = "http://petersdana98.000webhostapp.com/addSubjects.php";
    private static String urlconne = "http://petersdana98.000webhostapp.com/deleteStudent.php";
    private static final String TAG_SUBJECT = "subject";
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";

    private ProgressDialog pDialog;
    ListView listView;
    List<Subject> studentList = new ArrayList<>();

    Subject subject= null;
    SubjectAdapter subjectAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Geo Fencing");
        setSupportActionBar(toolbar);

        listView = (ListView) findViewById(R.id.lvStudent);

        stuNumber = getIntent().getStringExtra("stdnumber");

        new contactTask(stuNumber).execute();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

//        if (user != null) {
//
//            //View header = ((NavigationView)findViewById(R.id.nav_id)).getHeaderView(0);
//            View header = navigationView.getHeaderView(0);
//
//            TextView textView = (TextView) header.findViewById(R.id.textName);
//            //TextView textView = (TextView) navigationView.findViewById(R.id.textName);
//            textView.setText(user.getName() + " " + user.getSurname());
//            TextView textView1 = (TextView) header.findViewById(R.id.textEmail);
//            textView1.setText(email);
//        }

        BottomNavigationView bottomNavView = (BottomNavigationView) findViewById(R.id.bottomNavView_Bar);
        BottomNavigationViewHelper.disableShiftMode(bottomNavView);

        Menu menu = bottomNavView.getMenu();
        MenuItem menuItem = menu.getItem(0);
        menuItem.setChecked(true);

        bottomNavView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId())
                {
                    case R.id.ic_sarrow:

                        break;

                    case R.id.ic_sview:
                        Intent s_view = new Intent(StudentHome.this, SViewActivity.class);
                        s_view.putExtra("stdnumber",stuNumber);
                        startActivity(s_view);
                        break;

//                    case R.id.ic_ssearch:
//                        Intent ssearch = new Intent(StudentHome.this, SSearchActivity.class);
//                        ssearch.putExtra("stdnumber",stuNumber);
//                        startActivity(ssearch);
//                        break;
//
//                    case R.id.ic_supdate:
//                        Intent supdate = new Intent(StudentHome.this, SUpdateActivity.class);
//                        supdate.putExtra("stdnumber",stuNumber);
//                        startActivity(supdate);
//                        break;
                }

                return false;
            }
        });

       listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view,final int i, long l) {

                AlertDialog alertDialog = new AlertDialog.Builder(StudentHome.this).create();
                alertDialog.setTitle("Adding subject");
                alertDialog.setMessage("Add "+ studentList.get(i).getSubject()+" as part of your course");
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Add",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                               // Toast.makeText(StudentHome.this,"Hello "+studentList.get(i).getSubject(),Toast.LENGTH_LONG).show();
                                new AddContactTask(stuNumber,studentList.get(i).getStaffNumber(),studentList.get(i).getSubject(),studentList.get(i).getCode()).execute();
                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();

            }
        });
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_profile) {
            // Handle the camera action
            if (stuNumber != null ){
                Intent intent = new Intent(this, EditStudentProfileActivity.class);
                intent.putExtra("stdnumber", stuNumber);
                startActivity(intent);
                finish();
            }else
            {
                Toast.makeText(StudentHome.this,"No profile", Toast.LENGTH_SHORT).show();
            }
        }else if(id == R.id.nav_delete)
        {
            new deleteTask(stuNumber).execute();

        }else if(id == R.id.nav_logout)
        {
            startActivity(new Intent(this,MainActivity.class));
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.student_refresh, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.ic_refresh) {

            subjectAdapter.clear();
            subjectAdapter.notifyDataSetChanged();
            new contactTask(stuNumber).execute();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public class contactTask extends AsyncTask<String,String,String> {

        HttpURLConnection conn;
        URL url = null;
        String studentnumber;

        public contactTask(String stuNumber) {

            studentnumber = stuNumber;
        }


        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(StudentHome.this);
            pDialog.setMessage("Getting subjects...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... param) {

            try {

                // Enter URL address where your php file resides
                url = new URL(urlcon);

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return "exception";
            }
            try {
                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection)url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);
                conn.setDoOutput(true);

                // Append parameters to URL
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("student", studentnumber);
                String query = builder.build().getEncodedQuery();

                // Open connection for sending data
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                return "exception";
            }

            try {

                int response_code = conn.getResponseCode();

                Log.d("response_code",""+ response_code);
                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    Log.d("data",""+ result.toString());
                    // Pass data to onPostExecute method
                    return(result.toString());

                }else{

                    return("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }


        }

        @Override
        protected void onPostExecute(String success) {

            pDialog.dismiss();
            if (success != null){
                Log.i("Website Content", success);
                try {
                    JSONObject jsonPart = new JSONObject(success);
                    int num = jsonPart.getInt(TAG_SUCCESS);
                    JSONArray jsonArray = jsonPart.getJSONArray(TAG_SUBJECT);

                    if (num == 1) {

                        for(int x =0 ; x < jsonArray.length(); x++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(x);

                            //setAddresList(address);
                            String staffNumber = jsonObject.getString("staffNum");
                            String code = jsonObject.getString("code");
                            String subject = jsonObject.getString("subject");
                            Subject myContact = new Subject(staffNumber,code,subject);
                            if(myContact != null)
                            {
                                if (!studentList.contains(myContact)) {
                                    studentList.add(myContact);
                                }

                            }


                        }
                        Collections.sort(studentList, new Comparator<Subject>() {
                            public int compare(Subject obj1, Subject obj2) {
                                // ## Ascending order
                                return Integer.valueOf(obj1.getCode().compareToIgnoreCase(obj2.getCode()));
                                // return Integer.valueOf(obj1.getId).compareTo(obj2.getId); // To compare integer values

                            }
                        });

                        subjectAdapter = new SubjectAdapter(StudentHome.this,android.R.string.selectTextMode,studentList);
                        listView.setAdapter(subjectAdapter);
                        subjectAdapter.notifyDataSetChanged();



                        //new loadAccount().execute();

                    } else {

                        Toast.makeText(StudentHome.this, "Could not load subjects", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public class AddContactTask extends AsyncTask<String,String,String> {

        HttpURLConnection conn;
        URL url = null;
        String subject,staffnumber,studentnumber,code;

        public AddContactTask(String stuNumber, String staffNumber, String subject, String code) {
            this.staffnumber = staffNumber;
            this.subject = subject;
            studentnumber = stuNumber;
            this.code = code;
        }

        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(StudentHome.this);
            pDialog.setMessage("Adding subject...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... param) {

            try {

                // Enter URL address where your php file resides
                url = new URL(urlConne);

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return "exception";
            }
            try {
                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection)url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);
                conn.setDoOutput(true);

                // Append parameters to URL
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("studentnumber", stuNumber)
                        .appendQueryParameter("subject", subject)
                        .appendQueryParameter("staffnumber", staffnumber);
//                        .appendQueryParameter("code", code);
                String query = builder.build().getEncodedQuery();

                // Open connection for sending data
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                return "exception";
            }

            try {

                int response_code = conn.getResponseCode();

                Log.d("response_code",""+ response_code);
                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    Log.d("data",""+ result.toString());
                    // Pass data to onPostExecute method
                    return(result.toString());

                }else{

                    return("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }


        }

        @Override
        protected void onPostExecute(String success) {

            pDialog.dismiss();
            if (success != null){
                Log.i("Website Content", success);
                try {
                    JSONObject jsonPart = new JSONObject(success);
                    int num = jsonPart.getInt(TAG_SUCCESS);
                    String message = jsonPart.getString(TAG_MESSAGE);

                    if (num == 1) {


                        Intent intent = new Intent(StudentHome.this,SViewActivity.class);
                        intent.putExtra("stdnumber",stuNumber);
                        startActivity(intent);

                        Toast.makeText(StudentHome.this, "Subject successfully added", Toast.LENGTH_LONG).show();


                    } else {
                        // pDialog.dismiss();
                        Toast.makeText(StudentHome.this, message, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public class deleteTask extends AsyncTask<String,String,String> {

        HttpURLConnection conn;
        URL url = null;
        String username = "";

        public deleteTask(String phone) {

            username = phone;
        }


        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(StudentHome.this);
            pDialog.setMessage("Deactivating account...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            // TODO: attempt authentication against a network service.

            try {

                // Enter URL address where your php file resides
                url = new URL(urlconne);

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return "exception";
            }
            try {
                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection)url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);
                conn.setDoOutput(true);

                // Append parameters to URL
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("studentnumber", username);
                String query = builder.build().getEncodedQuery();

                // Open connection for sending data
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                return "exception";
            }

            try {

                int response_code = conn.getResponseCode();


                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    // Pass data to onPostExecute method
                    return(result.toString());

                }else{

                    return("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }

        }

        @Override
        protected void onPostExecute(String success) {

            if (success != null){
                Log.i("Hello",success);
                try {
//{"accoount":[{"name":"Lebo","surname":"Nkwana","id":"8802056549527","email":"bong@gmail.com","cellphone":"0748007959"}],"success":1}

                    JSONObject jsonPart = new JSONObject(success);
                    int num = jsonPart.getInt(TAG_SUCCESS);
                    String message = jsonPart.getString(TAG_SUCCESS);


                    // memoryInterface.setPersonContent(person);
                    if (num == 1) {

                        startActivity(new Intent(StudentHome.this,MainActivity.class));


                    } else {
                        Toast.makeText(StudentHome.this, message, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
