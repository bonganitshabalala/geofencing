package com.example.khumo.geofencing.Student;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.khumo.geofencing.BottomNavigationViewHelper;
import com.example.khumo.geofencing.Lecturer.ViewActivity;
import com.example.khumo.geofencing.R;
import com.example.khumo.geofencing.adapter.CourseAdapter;
import com.example.khumo.geofencing.adapter.SubjectAdapter;
import com.example.khumo.geofencing.pojo.Course;
import com.example.khumo.geofencing.pojo.Lecture;
import com.example.khumo.geofencing.pojo.Subject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Khumo on 9/26/2017.
 */

public class SViewActivity extends AppCompatActivity {

    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 10000;
    private static String urlcon = "http://petersdana98.000webhostapp.com/getCourseSubject.php";
    private static String urlConnection = "http://petersdana98.000webhostapp.com/removeSubject.php";
    private static final String TAG_SUBJECT = "course";
    private static final String TAG_SUCCESS = "success";
    private ProgressDialog pDialog;
    private static String phoneNumber;
    private EditText etCode,etSubject;
    private String code,subject;
    Activity activity;

    static String stuNumber;
    ListView listView;
    List<Course> courseList = new ArrayList<>();
    Lecture lecture;
    CourseAdapter courseAdapter;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sview);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        listView = (ListView) findViewById(R.id.lvStudent);

        stuNumber = getIntent().getStringExtra("stdnumber");

        new contactTask(stuNumber).execute();

        BottomNavigationView bottomNavView = (BottomNavigationView) findViewById(R.id.bottomNavView_Bar);
        BottomNavigationViewHelper.disableShiftMode(bottomNavView);

        Menu menu = bottomNavView.getMenu();
        MenuItem menuItem = menu.getItem(1);
        menuItem.setChecked(true);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {


                //Toast.makeText(SViewActivity.this," clicked "+courseList.get(i).getSubject(),Toast.LENGTH_SHORT).show();
                Intent sback = new Intent(SViewActivity.this, ViewTimeTableActivity.class);
                sback.putExtra("stdnumber",stuNumber);
                sback.putExtra("subject",courseList.get(i).getSubject());
                startActivity(sback);
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view,final int i, long l) {


                AlertDialog alertDialog = new AlertDialog.Builder(SViewActivity.this).create();
                alertDialog.setTitle("Remove subject");
                alertDialog.setMessage("Are you sure you want to remove subject");
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Remove",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // Toast.makeText(StudentHome.this,"Hello "+studentList.get(i).getSubject(),Toast.LENGTH_LONG).show();
                                new deleteTask(courseList.get(i).getSubject(),stuNumber).execute();
                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();

                return true;
            }
        });

        bottomNavView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId())
                {
                    case R.id.ic_sarrow:
                        Intent sback = new Intent(SViewActivity.this, StudentHome.class);
                        sback.putExtra("stdnumber",stuNumber);
                        startActivity(sback);
                        break;

                    case R.id.ic_sview:
                        break;

//                    case R.id.ic_ssearch:
//                        Intent ssearch = new Intent(SViewActivity.this, SSearchActivity.class);
//                        ssearch.putExtra("stdnumber",stuNumber);
//                        startActivity(ssearch);
//                        break;
//
//                    case R.id.ic_supdate:
//                        Intent supdate = new Intent(SViewActivity.this, SUpdateActivity.class);
//                        supdate.putExtra("stdnumber",stuNumber);
//                        startActivity(supdate);
//                        break;
                }

                return false;
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my_student, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.ic_refresh:

                courseAdapter.clear();
                courseAdapter.notifyDataSetChanged();
                new contactTask(stuNumber).execute();

                break;

            default:
                break;
        }

        return false;
    }


    /* *
 * Called when invalidateOptionsMenu() is triggered
 */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // if nav drawer is opened, hide the action items
//        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
//        menu.findItem(R.id.action_add).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }


    public class contactTask extends AsyncTask<String,String,String> {

        HttpURLConnection conn;
        URL url = null;
        String phone;

        public contactTask(String phoneNumber) {

            phone = phoneNumber;
        }

        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(SViewActivity.this);
            pDialog.setMessage("Getting subjects...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... param) {

            try {

                // Enter URL address where your php file resides
                url = new URL(urlcon);

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return "exception";
            }
            try {
                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection)url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);
                conn.setDoOutput(true);

                // Append parameters to URL
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("studentnumber", stuNumber);
                String query = builder.build().getEncodedQuery();

                // Open connection for sending data
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                return "exception";
            }

            try {

                int response_code = conn.getResponseCode();

                Log.d("response_code",""+ response_code);
                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    Log.d("data",""+ result.toString());
                    // Pass data to onPostExecute method
                    return(result.toString());

                }else{

                    return("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }


        }

        @Override
        protected void onPostExecute(String success) {

            pDialog.dismiss();
            if (success != null){
                Log.i("Website Content", success);
                try {
                    JSONObject jsonPart = new JSONObject(success);
                    int num = jsonPart.getInt(TAG_SUCCESS);
                    JSONArray jsonArray = jsonPart.getJSONArray(TAG_SUBJECT);

                    if (num == 1) {

                        for(int x =0 ; x < jsonArray.length(); x++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(x);

                            //setAddresList(address);
                            String subject = jsonObject.getString("subject");
                            String staff = jsonObject.getString("staff");
                            Course course = new Course(subject,staff);
                            if(course != null)
                            {
                                if (!courseList.contains(course)) {
                                    courseList.add(course);
                                }

                            }


                        }
                        Collections.sort(courseList, new Comparator<Course>() {
                            public int compare(Course obj1, Course obj2) {
                                // ## Ascending order
                                return Integer.valueOf(obj1.getSubject().compareToIgnoreCase(obj2.getSubject()));
                                // return Integer.valueOf(obj1.getId).compareTo(obj2.getId); // To compare integer values

                            }
                        });

                         courseAdapter = new CourseAdapter(SViewActivity.this,android.R.string.selectTextMode,courseList);
                        listView.setAdapter(courseAdapter);
                        courseAdapter.notifyDataSetChanged();



                        //new loadAccount().execute();

                    } else {

                        Toast.makeText(SViewActivity.this, "Could not load subjects", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public class deleteTask extends AsyncTask<String,String,String> {

        HttpURLConnection conn;
        URL url = null;
        String subject,studentNum;

        public deleteTask(String staff,String student) {

            subject = staff;
            studentNum = student;
        }

        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(SViewActivity.this);
            pDialog.setMessage("Removing subject...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... param) {

            try {

                // Enter URL address where your php file resides
                url = new URL(urlConnection);

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return "exception";
            }
            try {
                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection)url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);
                conn.setDoOutput(true);

                // Append parameters to URL
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("subject", subject)
                        .appendQueryParameter("student", studentNum);
                String query = builder.build().getEncodedQuery();

                // Open connection for sending data
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                return "exception";
            }

            try {

                int response_code = conn.getResponseCode();

                Log.d("response_code",""+ response_code);
                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    Log.d("data",""+ result.toString());
                    // Pass data to onPostExecute method
                    return(result.toString());

                }else{

                    return("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }


        }

        @Override
        protected void onPostExecute(String success) {

            pDialog.dismiss();
            if (success != null){
                Log.i("Website Content", success);
                try {
                    JSONObject jsonPart = new JSONObject(success);
                    int num = jsonPart.getInt(TAG_SUCCESS);


                    if (num == 1) {

                        Intent s_view = new Intent(SViewActivity.this, SViewActivity.class);
                        s_view.putExtra("stdnumber",stuNumber);
                        startActivity(s_view);

                    } else {

                        Toast.makeText(SViewActivity.this, "Could not remove subject", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
