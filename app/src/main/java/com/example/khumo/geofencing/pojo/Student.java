package com.example.khumo.geofencing.pojo;

/**
 * Created by Bongani on 2017/10/17.
 */

public class Student {


    private String stuNumber;
    private String name;
    private String surname;
    private String email;
    private String idNumber;
    private String gender;

    public Student() {
    }

    public Student(String stuNumber, String name, String surname, String email, String idNumber, String gender) {
        this.stuNumber = stuNumber;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.idNumber = idNumber;
        this.gender = gender;
    }

    public Student(String name, String surname, String email, String idNumber, String gender) {

        this.name = name;
        this.surname = surname;
        this.email = email;
        this.idNumber = idNumber;
        this.gender = gender;
    }

    public Student(String name, String surname, String idNumber) {

        this.name = name;
        this.surname = surname;
        this.idNumber = idNumber;

    }

    public String getStuNumber() {
        return stuNumber;
    }

    public void setStuNumber(String stuNumber) {
        this.stuNumber = stuNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
