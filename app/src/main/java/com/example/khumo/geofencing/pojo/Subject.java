package com.example.khumo.geofencing.pojo;

/**
 * Created by Bongani on 2017/10/16.
 */

public class Subject {

    private String code;
    private String subject;
    private String staffNumber;

    public Subject() {
    }

    public Subject(String code, String subject) {
        this.code = code;
        this.subject = subject;
    }

    public Subject(String staffNumber, String code, String subject) {
        this.code = code;
        this.subject = subject;
        this.staffNumber = staffNumber;
    }

    public String getStaffNumber() {
        return staffNumber;
    }

    public void setStaffNumber(String staffNumber) {
        this.staffNumber = staffNumber;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}
