package com.example.khumo.geofencing.pojo;

/**
 * Created by Bongani on 2017/10/16.
 */

public class Lecture {

    private String staffNumber;
    private String name;
    private String surname;
    private String email;
    private String idNumber;
    private String gender;

    public Lecture() {
    }

    public Lecture(String staffNumber, String name, String surname, String email, String idNumber, String gender) {
        this.staffNumber = staffNumber;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.idNumber = idNumber;
        this.gender = gender;
    }

    public Lecture(String name, String surname, String email, String idNumber, String gender) {

        this.name = name;
        this.surname = surname;
        this.email = email;
        this.idNumber = idNumber;
        this.gender = gender;
    }

    public String getStaffNumber() {
        return staffNumber;
    }

    public void setStaffNumber(String staffNumber) {
        this.staffNumber = staffNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
