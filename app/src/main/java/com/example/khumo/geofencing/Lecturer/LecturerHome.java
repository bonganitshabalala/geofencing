package com.example.khumo.geofencing.Lecturer;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.khumo.geofencing.BottomNavigationViewHelper;
import com.example.khumo.geofencing.helper.FileDialog;
import com.example.khumo.geofencing.FinishedActivity;
import com.example.khumo.geofencing.MainActivity;
import com.example.khumo.geofencing.R;
import com.example.khumo.geofencing.helper.Utils;
import com.example.khumo.geofencing.adapter.SubjectAdapter;
import com.example.khumo.geofencing.fragment.CreateTimeslotDialogFragment;
import com.example.khumo.geofencing.fragment.SubjectDialogFragment;
import com.example.khumo.geofencing.pojo.Lecture;
import com.example.khumo.geofencing.pojo.Subject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class LecturerHome extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    private String staffNumber;
    NavigationView navigationView;
    private FloatingActionButton btnAdd;
    ListView listView;

    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 10000;
    private static String urlcon = "http://petersdana98.000webhostapp.com/getSubject.php";
    private static String urlconne = "http://petersdana98.000webhostapp.com/deleteLecturer.php";
    private static final String TAG_SUBJECT = "subject";
    private static String phoneNumber;
    private EditText etCode,etSubject;
    private String code,subject;
    Activity activity;
    private ProgressDialog pDialog;
    static String pass;
    List<Subject> lectureList = new ArrayList<>();
    Lecture lecture;
    private static final String FTYPE = ".csv";
    private static final int DIALOG_LOAD_FILE = 1000;
    File hostFile,sdcFile;
    private static final String logDir = "/LogData";


    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lecturer_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Geo Fencing");
        setSupportActionBar(toolbar);

        staffNumber = getIntent().getStringExtra("staff");
        Log.w("TAG", "da "+staffNumber);

        listView = (ListView) findViewById(R.id.listView);


       // new contactTask(staffNumber).execute();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        btnAdd = (FloatingActionButton)findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addSubject();
            }
        });

        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavView_Bar);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);

        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(0);
        menuItem.setChecked(true);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId())
                {
                    case R.id.ic_arrow:

                        break;

                    case R.id.ic_view:
                        Intent view = new Intent(LecturerHome.this, ViewActivity.class);
                        view.putExtra("staff",staffNumber);
                        startActivity(view);
                        break;

                    case R.id.ic_search:
                        Intent search = new Intent(LecturerHome.this, SearchActivity.class);
                        search.putExtra("staff",staffNumber);
                        startActivity(search);
                        break;

//                   break case R.id.ic_update:
//                        Intent update = new Intent(LecturerHome.this, UpdateActivity.class);
//                        update.putExtra("staff",staffNumber);
//                        startActivity(update);
//                        break;
//
//                    case R.id.ic_delete:
//                        Intent delete = new Intent(LecturerHome.this, DeleteActivity.class);
//                        delete.putExtra("staff",staffNumber);
//                        startActivity(delete);
//                        break;
                }

                return false;
            }
        });
    }

    private void addSubject() {


            SubjectDialogFragment logOptionsDialogFragment = SubjectDialogFragment.newInstance(staffNumber);
            android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.add(logOptionsDialogFragment,"newFragment");
            ft.commit();


    }


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//
//            case R.id.ic_delete:
//
//
//                break;
//
//            default:
//                break;
//        }
//
//        return false;
//    }


    /* *
 * Called when invalidateOptionsMenu() is triggered
 */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // if nav drawer is opened, hide the action items
//        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
//        menu.findItem(R.id.action_add).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_profile) {
            // Handle the camera action
            if (staffNumber != null ){
                Intent intent = new Intent(this, EditprofileActivity.class);
                intent.putExtra("staff", staffNumber);
                startActivity(intent);
                finish();
            }else
            {
                Toast.makeText(LecturerHome.this,"No profile", Toast.LENGTH_SHORT).show();
            }
        }else if(id == R.id.nav_create)
        {
            CreateTimeslotDialogFragment logOptionsDialogFragment = CreateTimeslotDialogFragment.newInstance(staffNumber,false);
            android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.add(logOptionsDialogFragment,"newFragment");
            ft.commit();
        }else if(id == R.id.nav_delete)
        {
            new deleteTask(staffNumber).execute();


        }else if(id == R.id.nav_logout)
        {
            startActivity(new Intent(this,MainActivity.class));
            finish();
        }else if(id == R.id.nav_timeslot)
        {
            Intent i = new Intent(LecturerHome.this,FinishedActivity.class);
            i.putExtra("staff",staffNumber);
            startActivity(i);
        }else if(id == R.id.nav_record)
        {

            Intent i = new Intent(LecturerHome.this,ReportViewActivity.class);
            i.putExtra("staff",staffNumber);
            startActivity(i);
//            File temp_file=new File("myfile.csv");
//
//            if (Utils.isExternalStorageAvailable() || !Utils.isExternalStorageReadOnly()) {
//
//                sdcFile = new File(Environment.getExternalStorageDirectory()+ logDir);
//                if(sdcFile.exists()) {
//                    FileDialog fileDialog = new FileDialog(this, sdcFile);
//                    fileDialog.addFileListener(new FileDialog.FileSelectedListener() {
//                        public void fileSelected(File file) {
//                            Log.d(getClass().getName(), "selected file " + file.getName());
//
//                            Intent intent = new Intent();
//                            intent.setAction(android.content.Intent.ACTION_VIEW);
//                            intent.setDataAndType(Uri.fromFile(file),getMimeType(file.getAbsolutePath()));
//                            startActivity(intent);
//
//                        }
//                    });
//                    fileDialog.showDialog();
//                }
//
//            }


        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private String getMimeType(String url)
    {
        String parts[]=url.split("\\.");
        String extension=parts[parts.length-1];
        String type = null;
        if (extension != null) {
            MimeTypeMap mime = MimeTypeMap.getSingleton();
            type = mime.getMimeTypeFromExtension(extension);
        }
        return type;
    }

    public class contactTask extends AsyncTask<String,String,String> {

        HttpURLConnection conn;
        URL url = null;
        String phone;

        public contactTask(String phoneNumber) {

            phone = phoneNumber;
        }

        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(LecturerHome.this);
            pDialog.setMessage("Getting subjects...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... param) {

            try {

                // Enter URL address where your php file resides
                url = new URL(urlcon);

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return "exception";
            }
            try {
                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection)url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);
                conn.setDoOutput(true);

                // Append parameters to URL
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("staff", phone);
                String query = builder.build().getEncodedQuery();

                // Open connection for sending data
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                return "exception";
            }

            try {

                int response_code = conn.getResponseCode();

                Log.d("response_code",""+ response_code);
                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    Log.d("data",""+ result.toString());
                    // Pass data to onPostExecute method
                    return(result.toString());

                }else{

                    return("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }


        }

        @Override
        protected void onPostExecute(String success) {

            pDialog.dismiss();
            if (success != null){
                Log.i("Website Content", success);
                try {
                    JSONObject jsonPart = new JSONObject(success);
                    int num = jsonPart.getInt(TAG_SUCCESS);
                    JSONArray jsonArray = jsonPart.getJSONArray(TAG_SUBJECT);

                    if (num == 1) {

                        for(int x =0 ; x < jsonArray.length(); x++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(x);

                            //setAddresList(address);
                            String code = jsonObject.getString("code");
                            String subject = jsonObject.getString("subjectname");
                            Subject myContact = new Subject(code,subject);
                            if(myContact != null)
                            {
                                    if (!lectureList.contains(myContact)) {
                                        lectureList.add(myContact);
                                    }

                            }


                        }

                        SubjectAdapter subjectAdapter = new SubjectAdapter(LecturerHome.this,android.R.string.selectTextMode,lectureList);
                        listView.setAdapter(subjectAdapter);



                        //new loadAccount().execute();

                    } else {

                        //Toast.makeText(MainsActivity.this, message, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public class deleteTask extends AsyncTask<String,String,String> {

        HttpURLConnection conn;
        URL url = null;
        String username = "";

        public deleteTask(String phone) {

            username = phone;
        }


        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(LecturerHome.this);
            pDialog.setMessage("Deactivating account...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            // TODO: attempt authentication against a network service.

            try {

                // Enter URL address where your php file resides
                url = new URL(urlconne);

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return "exception";
            }
            try {
                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection)url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);
                conn.setDoOutput(true);

                // Append parameters to URL
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("staff", username);
                String query = builder.build().getEncodedQuery();

                // Open connection for sending data
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                return "exception";
            }

            try {

                int response_code = conn.getResponseCode();


                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    // Pass data to onPostExecute method
                    return(result.toString());

                }else{

                    return("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }

        }

        @Override
        protected void onPostExecute(String success) {

            pDialog.dismiss();
            if (success != null){
                Log.i("Hello",success);
                try {
//{"accoount":[{"name":"Lebo","surname":"Nkwana","id":"8802056549527","email":"bong@gmail.com","cellphone":"0748007959"}],"success":1}

                    JSONObject jsonPart = new JSONObject(success);
                    int num = jsonPart.getInt(TAG_SUCCESS);
                    String message = jsonPart.getString(TAG_SUCCESS);


                    // memoryInterface.setPersonContent(person);
                    if (num == 1) {

                        startActivity(new Intent(LecturerHome.this,MainActivity.class));
                        finish();

                    } else {
                        Toast.makeText(LecturerHome.this, message, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
