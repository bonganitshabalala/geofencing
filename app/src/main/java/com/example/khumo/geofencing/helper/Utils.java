package com.example.khumo.geofencing.helper;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by Bongani on 2016/11/10.
 */
public class Utils {


    public Utils(Activity activity){
        mActivity = activity;
        mUiThreadId = Thread.currentThread().getId();
    }

    List<File> inFileNames= new ArrayList<File>();

    public static boolean isExternalStorageReadOnly() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(extStorageState)) {
            return true;
        }
        return false;
    }

    public static boolean isExternalStorageAvailable() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(extStorageState)) {
            return true;
        }
        return false;
    }

    public static boolean copyFileData(File src,File dst)throws IOException {

        Log.e("LOcation :: ", "source "+ src + " disto " + dst);
        if(src.getAbsolutePath().toString().equals(dst.getAbsolutePath().toString())){

            return true;

        }else{
            InputStream is=new FileInputStream(src);
            OutputStream os=new FileOutputStream(dst);
            byte[] buff=new byte[1024];
            int len;
            while((len=is.read(buff))>0){
                os.write(buff,0,len);
            }
            is.close();
            os.close();
        }
        return true;
    }

    public static List<String> setFileContent(Activity activity, File mFile) {

        List<String> list = new ArrayList<>();
        if(mFile.exists()) {
            File[] files = mFile.listFiles();

            try {
                if (files.length > 0) {

                    for (File file : files) {
                        Log.d("TAG", "File name " + file);
                        if (file.getName().endsWith(".mp4")) {
                            list.add(file.getName());
                        }else if(file.getName().endsWith(".mp3"))
                        {
                            list.add(file.getName());
                        }


                    }
                }


            } catch (Exception e) {
                e.getMessage();
            }
        }else
        {
            list = new ArrayList<>();
            Toast.makeText(activity, "No media files", Toast.LENGTH_SHORT).show();
        }

        return list;


    }

    //**********************************************************************************************

    private Activity mActivity = null;
    private long mUiThreadId = -1;

    public void init(Activity activity) {
        mActivity = activity;
        mUiThreadId = Thread.currentThread().getId();
    }

    private enum ShowDisplay {

        show     ("Playing video.....");
        ShowDisplay(String text){
            display = text;
        }
        private String display;

        @Override
        public String toString() {
            return display;
        }
    }

    public void toast(final String message){
        if (Thread.currentThread().getId() == mUiThreadId) {
            Toast.makeText(mActivity, message, Toast.LENGTH_SHORT).show();
        } else {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(mActivity, message, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    public static String getDateTime(Date timestamp){
        DateFormat dateformat = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT);
        dateformat.setTimeZone(TimeZone.getDefault());
        String timeString = timestamp != null ? dateformat.format(timestamp) : "N/A";
        return timeString;
    }
    static int number;
    public static void setCount(int numbers)
    {
       number = numbers;

    }

    public static int getCount()
    {

        return number;
    }




}
