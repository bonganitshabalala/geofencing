package com.example.khumo.geofencing.Student;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.khumo.geofencing.R;
import com.example.khumo.geofencing.pojo.Student;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Pattern;

/**
 * Created by Bongani on 2017/10/17.
 */

public class EditStudentProfileActivity extends AppCompatActivity implements TextWatcher {


    private EditText mEmailView;
    private EditText mName;
    private EditText mSurname;
    private EditText mID;
    private EditText mGender;

    String email;
    String name  ;
    String surname ;
    String idNumber;
    String gender;


    private ProgressDialog pDialog;
    private static String urlz = "http://petersdana98.000webhostapp.com/getStudent.php";
    private static String urlCon = "http://petersdana98.000webhostapp.com/updateStudent.php";


    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";
    private static final String TAG_USER = "lec";
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 10000;

    String uuid = "";
    Student student;
    String staff;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_student_profile);
        // Set up the login form.
         Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
          setSupportActionBar(toolbar);

        mName = (EditText) findViewById(R.id.et_Fname);
        mSurname = (EditText) findViewById(R.id.et_Lname);
        mEmailView = (EditText) findViewById(R.id.et_Email);
        mID = (EditText) findViewById(R.id.et_IDnum);
        mGender = (EditText) findViewById(R.id.et_Gender);


        mName.addTextChangedListener(this);
        mSurname.addTextChangedListener(this);


        staff = getIntent().getStringExtra("stdnumber");
//        DatabaseHandler db = new DatabaseHandler(this);
//
//        User user = db.getUser(email);
        new statusTask(staff).execute();



        Button btnRegister = (Button) findViewById(R.id.btnUpdate);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptRegistration();
            }
        });

        Button btnBack = (Button) findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(EditStudentProfileActivity.this,StudentHome.class);
                intent.putExtra("stdnumber",staff);
                startActivity(intent);
            }
        });



    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.edit_menu, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_changepassword) {
//
//            if (user != null){
//                changePassword(user.getPassword(),user.getEmail());
//            }
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    private boolean isOnline(Context mContext) {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        boolean valid;
        String emailPattern = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        if (Pattern.matches(emailPattern, email))
        {

            valid = true;


        }else {
            valid = false;
        }

        return valid;
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic

        return password.length() > 4;
    }

    private void attemptRegistration() {

        // Store values at the time of the login attempt.
        name = mName.getText().toString();
        surname = mSurname.getText().toString();
        email = mEmailView.getText().toString();
        idNumber = mID.getText().toString();

        boolean cancel = false;
        View focusView = null;


        if (TextUtils.isEmpty(name)) {
            mName.setError("Error - name can't be empty");
            focusView = mName;
            cancel = true;

        }

        if (TextUtils.isEmpty(surname)) {
            mSurname.setError("Error - surname can't be empty");
            focusView = mSurname;
            cancel = true;

        }



        if (TextUtils.isEmpty(email)) {
            mEmailView.setError("Error - email can't be empty");
            focusView = mEmailView;
            cancel = true;

        }

        if (!TextUtils.isEmpty(email)) {
            if (!isEmailValid(email)) {
                mEmailView.setError("Error - Enter correct email");
                focusView = mEmailView;
                cancel = true;

            }
        }


        if (TextUtils.isEmpty(idNumber)) {
            mID.setError("Error - ID number can't be empty");
            focusView = mID;
            cancel = true;

        }

        if (!TextUtils.isEmpty(idNumber)) {
            if (!isIDValid(idNumber)) {
                mID.setError("Enter correct ID number");
                focusView = mID;
                cancel = true;

            }
        }


        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            if (!isOnline(EditStudentProfileActivity.this)) {
                Toast.makeText(EditStudentProfileActivity.this, "No internet connection", Toast.LENGTH_LONG).show();
            } else {

                new RegistrationTask().execute();



            }
        }
    }

    private boolean isIDValid(String number) {
        //TODO: Replace this with your own logic
        String preg = "([0-9]){2}([0-1][0-9])([0-3][0-9])([0-9]){4}([0-1])([8]){1}([0-9]){1}?";
        boolean valid;
        if (Pattern.matches(preg, number))
        {
            if(number.length() == 13)
            {
                valid = true;
            }else
            {
                valid = false;
            }

        }else {
            valid = false;
        }



        return valid;
    }

    public  String getSex() {
        String M = "Male", F = "Female";

        int d = Integer.parseInt(idNumber.substring(6, 7));
        if (d <= 4 || d == 0) {
            return F;
        } else {
            return M;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {



        name = mName.getText().toString();
        if(name.contains(" "))
        {
            mName.setText(name.replace(" ","-"));
            mName.setSelection(name.length());
        }

        surname = mSurname.getText().toString();
        if(surname.contains(" "))
        {
            mSurname.setText(surname.replace(" ","-"));
            mSurname.setSelection(surname.length());
        }


    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class RegistrationTask extends AsyncTask<String,String,String> {

        HttpURLConnection conn;
        URL url = null;
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(EditStudentProfileActivity.this);
            pDialog.setMessage("Updating account...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... param) {

            try {

                // Enter URL address where your php file resides
                url = new URL(urlCon);

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return "exception";
            }
            try {
                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection)url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);
                conn.setDoOutput(true);

                // Append parameters to URL
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("name", name)
                        .appendQueryParameter("surname", surname)
                        .appendQueryParameter("email", email)
                        .appendQueryParameter("idnumber", idNumber)
                        .appendQueryParameter("gender", getSex())
                        .appendQueryParameter("staff", staff);
                String query = builder.build().getEncodedQuery();

                // Open connection for sending data
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                return "exception";
            }

            try {

                int response_code = conn.getResponseCode();

                Log.d("response_code",""+ response_code);
                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    Log.d("data",""+ result.toString());
                    // Pass data to onPostExecute method
                    return(result.toString());

                }else{

                    return("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }


        }

        @Override
        protected void onPostExecute(String success) {

            pDialog.dismiss();
            if (success != null){
                Log.i("Website Content", success);
                try {
                    JSONObject jsonPart = new JSONObject(success);
                    int num = jsonPart.getInt(TAG_SUCCESS);
                    String message = jsonPart.getString(TAG_MESSAGE);

                    if (num == 1) {

                        //save sharedpreferences
                        Intent intent = new Intent(EditStudentProfileActivity.this,StudentHome.class);
                        intent.putExtra("stdnumber",staff);
                        startActivity(intent);
                        Toast.makeText(EditStudentProfileActivity.this, "Profile updated successfully", Toast.LENGTH_LONG).show();

                    } else {
                        // pDialog.dismiss();
                        Toast.makeText(EditStudentProfileActivity.this, message, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    @Override
    protected void onDestroy() {
        //  stopService(mServiceIntent);
        Log.i("MAINACT", "onDestroy!");
        super.onDestroy();

    }

    public class statusTask extends AsyncTask<String,String,String> {

        HttpURLConnection conn;
        URL url = null;
        String email;

        public statusTask(String emails) {
            email = emails;
        }

        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(EditStudentProfileActivity.this);
            pDialog.setMessage("Getting account...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... param) {

            try {

                // Enter URL address where your php file resides
                url = new URL(urlz);

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return "exception";
            }
            try {
                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection)url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);
                conn.setDoOutput(true);

                // Append parameters to URL
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("staff", staff);
                String query = builder.build().getEncodedQuery();

                // Open connection for sending data
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                return "exception";
            }

            try {

                int response_code = conn.getResponseCode();

                Log.d("response_code",""+ response_code);
                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    Log.d("data",""+ result.toString());
                    // Pass data to onPostExecute method
                    return(result.toString());

                }else{

                    return("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }


        }

        @Override
        protected void onPostExecute(String success) {

            pDialog.dismiss();
            if (success != null){
                Log.i("Website Content", success);
                try {
                    JSONObject jsonPart = new JSONObject(success);
                    int num = jsonPart.getInt(TAG_SUCCESS);
                    JSONArray jsonArray = jsonPart.getJSONArray(TAG_USER);

                    if (num == 1) {

                        for(int x =0 ; x < jsonArray.length(); x++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(x);

                            //setAddresList(address);

                            String name = jsonObject.getString("name");
                            String surname = jsonObject.getString("surname");
                            String email = jsonObject.getString("email");
                            String idnumber = jsonObject.getString("idnumber");
                            String gender = jsonObject.getString("gender");

                            student = new Student(name,surname,email,idnumber,gender);
                            if(student != null)
                            {

                                mName.setText(student.getName());
                                mSurname.setText(student.getSurname());
                                mEmailView.setText(student.getEmail());
                                //mEmailView.setEnabled(false);
                                mID.setText(student.getIdNumber());
                              //  mID.setEnabled(false);
                                mGender.setText(student.getGender());
                                mGender.setEnabled(false);
                            }else
                            {
                                ((Button)findViewById(R.id.btnUpdate)).setEnabled(false);
                            }


                        }

                        //new loadAccount().execute();

                    } else {

                        //Toast.makeText(MainsActivity.this, message, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }



}
