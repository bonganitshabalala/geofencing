package com.example.khumo.geofencing.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.khumo.geofencing.Lecturer.ViewActivity;
import com.example.khumo.geofencing.Lecturer.ViewStudentAttendance;
import com.example.khumo.geofencing.R;
import com.example.khumo.geofencing.helper.Utils;
import com.example.khumo.geofencing.adapter.StudentAdapter;
import com.example.khumo.geofencing.adapter.StudentLogAdapter;
import com.example.khumo.geofencing.pojo.Student;
import com.opencsv.CSVWriter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by Bongani on 2017/10/17.
 */

public class ViewStudentDialogFragment extends DialogFragment {

    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 10000;
    private static String urlConne = "http://petersdana98.000webhostapp.com/getSubjectCourse.php";
    private static String urlz = "http://app-1505651429.000webhostapp.com/getContact.php";
    private static String urlConnection = "http://petersdana98.000webhostapp.com/removeStudent.php";
    private static final String TAG_USER = "lec";
    private static String phoneNumber;
    private EditText etCode,etSubject;
    private String stuNumber;
    static String subject;
    Activity activity;
    private ProgressDialog pDialog;
    static String pass;
    static String staffnumber;
    String studentnumber;
    ListView listView;
    List<Student> studentList = new ArrayList<>();

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";
    private static final String logDir = "/LogData";
    private static final String logFile = "/Class list";

    public static ViewStudentDialogFragment newInstance(String staff,String subj) {

        Bundle bundle = new Bundle();
        staffnumber = staff;
        subject = subj;
        ViewStudentDialogFragment sampleFragment = new ViewStudentDialogFragment();
        sampleFragment.setArguments(bundle);
        return sampleFragment;
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.view_student_course, null);
            Toolbar toolbar = (Toolbar) dialogView.findViewById(R.id.toolbar);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                toolbar.setTitle("Classroom list");
            }
            //updateView(dialogView);
            final Bundle args = getArguments();
            Log.w("TAG", "In here "+ staffnumber + " ss "+ subject);

            listView = (ListView)dialogView.findViewById(R.id.listView);


            new contactTask(staffnumber,subject).execute();

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {


                  Student  stuNumber =  studentList.get(i);

                    Log.w("TAG"," std "+ stuNumber.getStuNumber()+ " "+ studentList.get(i));


                    Intent intent = new Intent(getActivity(), ViewStudentAttendance.class);
                    intent.putExtra("studentnumber",stuNumber.getStuNumber());
                    intent.putExtra("subject",subject);
                    intent.putExtra("staff",staffnumber);
                    getActivity().startActivity(intent);



                }
            });

            listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                    final Student  stuNumber =  studentList.get(i);

                    AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                    alertDialog.setTitle("Remove student");
                    alertDialog.setMessage("Are you sure you want to remove student");
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Remove",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // Toast.makeText(StudentHome.this,"Hello "+studentList.get(i).getSubject(),Toast.LENGTH_LONG).show();
                                    new deleteTask(staffnumber,stuNumber.getStuNumber(),subject).execute();
                                }
                            });
                    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();

                    return true;
                }
            });



            final AlertDialog alertDialog = new AlertDialog.Builder(getActivity(), R.style.MyWhiteAlertDialogStyle)
                    .setView(dialogView)
                    .setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            Intent i = new Intent(getActivity(),ViewActivity.class);
                            i.putExtra("staff",staffnumber);
                            startActivity(i);
                        }
                    })
                    .setPositiveButton("Save classroom list", null)
                    .create();
            alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(final DialogInterface dialog) {
                    Button b = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
                    b.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            makeLog();


                        }
                    });
                }
            });
            return alertDialog;
        } else {
            return super.onCreateDialog(savedInstanceState);
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Do something that differs the Activity's menu here
        inflater.inflate(R.menu.student_refresh_view, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.ic_refresh) {

            new contactTask(staffnumber,subject).execute();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class contactTask extends AsyncTask<String,String,String> {

        HttpURLConnection conn;
        URL url = null;
        String staffNum,subject;

        public contactTask(String staff,String subj) {

            staffNum = staff;
            subject = subj;
        }

        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Getting students...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... param) {

            try {

                // Enter URL address where your php file resides
                url = new URL(urlConne);

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return "exception";
            }
            try {
                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection)url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);
                conn.setDoOutput(true);

                // Append parameters to URL
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("staff", staffNum)
                        .appendQueryParameter("subject", subject);
                String query = builder.build().getEncodedQuery();

                // Open connection for sending data
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                return "exception";
            }

            try {

                int response_code = conn.getResponseCode();

                Log.d("response_code",""+ response_code);
                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    Log.d("data",""+ result.toString());
                    // Pass data to onPostExecute method
                    return(result.toString());

                }else{

                    return("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }


        }

        @Override
        protected void onPostExecute(String success) {

            pDialog.dismiss();
            if (success != null){
                Log.i("Website Content", success);
                try {
                    JSONObject jsonPart = new JSONObject(success);
                    int num = jsonPart.getInt(TAG_SUCCESS);
                    JSONArray jsonArray = jsonPart.getJSONArray(TAG_USER);

                    if (num == 1) {

                        for(int x =0 ; x < jsonArray.length(); x++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(x);

                            //setAddresList(address);
                            String name = jsonObject.getString("name");
                            String surname = jsonObject.getString("surname");
                           String studentnumber = jsonObject.getString("studentnumber");


                            Student student = new Student(name,surname,studentnumber);
                            if(student != null)
                            {
                                if (!studentList.contains(student)) {
                                    studentList.add(student);
                                }

                            }


                        }

//                        BindDictionary<Student> dict = new BindDictionary<Student>();
//                        FunDapter adapter = new FunDapter<>(getActivity(), (ArrayList<Student>) studentList, R.layout.layout_list, dict);
//                        listView.setAdapter(adapter);
                        StudentAdapter subjectAdapter = new StudentAdapter(getActivity(),android.R.string.selectTextMode,studentList);
                        listView.setAdapter(subjectAdapter);



                        //new loadAccount().execute();

                    } else {

                        Toast.makeText(getActivity(), "No students available", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public class deleteTask extends AsyncTask<String,String,String> {

        HttpURLConnection conn;
        URL url = null;
        String staffNum,studentNum,sub;

        public deleteTask(String staff, String student, String subject) {

            staffNum = staff;
            studentNum = student;
            sub = subject;
        }

        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Removing student...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... param) {

            try {

                // Enter URL address where your php file resides
                url = new URL(urlConnection);

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return "exception";
            }
            try {
                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection)url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);
                conn.setDoOutput(true);

                // Append parameters to URL
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("staff", staffNum)
                        .appendQueryParameter("student", studentNum)
                        .appendQueryParameter("subject", sub);
                String query = builder.build().getEncodedQuery();

                // Open connection for sending data
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                return "exception";
            }

            try {

                int response_code = conn.getResponseCode();

                Log.d("response_code",""+ response_code);
                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    Log.d("data",""+ result.toString());
                    // Pass data to onPostExecute method
                    return(result.toString());

                }else{

                    return("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }


        }

        @Override
        protected void onPostExecute(String success) {

            pDialog.dismiss();
            if (success != null){
                Log.i("Website Content", success);
                try {
                    JSONObject jsonPart = new JSONObject(success);
                    int num = jsonPart.getInt(TAG_SUCCESS);
                    JSONArray jsonArray = jsonPart.getJSONArray(TAG_USER);

                    if (num == 1) {

                        ViewStudentDialogFragment logOptionsDialogFragment = ViewStudentDialogFragment.newInstance(staffNum,subject);
                        android.support.v4.app.FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                        ft.add(logOptionsDialogFragment,"newFragment");
                        ft.commit();

                    } else {

                        Toast.makeText(getActivity(), "Could not remove student", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public void makeLog() {
        try {

            CSVWriter writer;
            final ListView listView = new ListView(getActivity());
            listView.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);


            // int tot = booksDatabaseManipulation.getContactsCount(subject);
            // Log.w("TSAG","toal "+tot);
            //  logEntries = booksDatabaseManipulation.getContact(subject);
            final StudentLogAdapter logAdapter = new StudentLogAdapter(getActivity(), R.id.textView, studentList);
            listView.setAdapter(logAdapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    logAdapter.notifyDataSetChanged();
                }
            });

            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity(), R.style.MyWhiteAlertDialogStyle);
            builder.setTitle("Activities Log - ");
            builder.setView(listView)
                    .setNeutralButton("Store Log", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //Write Raw Frame Data to File

                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        boolean success = false;
                                        File dir = new File(Environment.getExternalStorageDirectory()+ logDir);
                                        dir.setReadable(true,false);

                                        success = (dir.mkdir() || dir.isDirectory());
                                        if (success) {
                                            String timelabel = "_" + Utils.getDateTime(new Date()).replace(" ", "_").replace(":", "h").replace("/","-");
                                            CSVWriter writer = new CSVWriter(new FileWriter(dir+ subject+ logFile + timelabel+".csv"), ',');
                                            FileOutputStream outputStream = new FileOutputStream(dir + logFile + timelabel + ".csv", false);
                                            // Runtime.getRuntime().exec("chmod 444 " + dir + logFile + timelabel + ".txt");
                                            String logString = "";
                                            // ProgressDialogStart("Application Log", "Storing app log in " + logDir + logFile + timelabel + ".txt...");
                                            for (ListIterator<Student> i = studentList.listIterator(); i.hasNext(); ) {
                                                Student entry = i.next();

                                                //  logString += entry.getWeekday() + ":"+ entry.getVenue()+":"+entry.getSubject();
                                                // logString += System.getProperty("line.separator");
                                                String[] entries = (entry.getStuNumber() + " : "+entry.getName() +" : "+entry.getSurname()).split(":");
                                                writer.writeNext(entries);
                                            }
                                            writer.close();
                                            Toast.makeText(getActivity(),"Successfully created file",Toast.LENGTH_SHORT).show();



                                        } else {
                                            Toast.makeText(getActivity(),"Could not create directory \"/LogData\"",Toast.LENGTH_SHORT).show();
                                        }

                                        //printLog("Log written to file...");
                                    } catch (Exception e) {
                                        e.getStackTrace();
                                    }
                                }
                            }, "writeFileThread").start();

                        }
                    })
                    .setNegativeButton("Close", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (dialog != null)
                                dialog.dismiss();
                        }
                    });
            android.support.v7.app.AlertDialog dialog = builder.create();
            dialog.show();
            listView.smoothScrollToPosition(logAdapter.getCount()-1);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        } catch (Exception e){
            e.getStackTrace();
        }
    }


}
