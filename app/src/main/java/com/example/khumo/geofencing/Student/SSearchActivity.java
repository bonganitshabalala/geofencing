package com.example.khumo.geofencing.Student;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.example.khumo.geofencing.BottomNavigationViewHelper;
import com.example.khumo.geofencing.R;

/**
 * Created by Khumo on 9/26/2017.
 */

public class SSearchActivity extends AppCompatActivity {

    private String stuNumber;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ssearch);

        stuNumber = getIntent().getStringExtra("stdnumber");

        BottomNavigationView bottomNavView = (BottomNavigationView) findViewById(R.id.bottomNavView_Bar);
        BottomNavigationViewHelper.disableShiftMode(bottomNavView);

        Menu menu = bottomNavView.getMenu();
        MenuItem menuItem = menu.getItem(2);
        menuItem.setChecked(true);

        bottomNavView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId())
                {
                    case R.id.ic_sarrow:
                        Intent sback = new Intent(SSearchActivity.this, StudentHome.class);
                        sback.putExtra("stdnumber",stuNumber);
                        startActivity(sback);
                        break;

                    case R.id.ic_sview:
                        Intent sview = new Intent(SSearchActivity.this, SViewActivity.class);
                        sview.putExtra("stdnumber",stuNumber);
                        startActivity(sview);
                        break;

//                    case R.id.ic_ssearch:
//
//                        break;
//
//                    case R.id.ic_supdate:
//                        Intent ssearch = new Intent(SSearchActivity.this, SUpdateActivity.class);
//                        ssearch.putExtra("stdnumber",stuNumber);
//                        startActivity(ssearch);
//                        break;
                }

                return false;
            }
        });
    }
}
