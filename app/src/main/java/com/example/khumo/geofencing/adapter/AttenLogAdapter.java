package com.example.khumo.geofencing.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.khumo.geofencing.R;
import com.example.khumo.geofencing.pojo.Attendance;

import java.util.List;

/**
 * Created by Bongani on 2017/11/02.
 */

public class AttenLogAdapter extends ArrayAdapter<Attendance> {

    private Activity mContext = null;
    private LayoutInflater mInflater = null;

    private static class ViewHolder {
        private TextView tvTime = null;
        private TextView tvSub = null;
        private TextView tvType = null;
        private TextView tvPage = null;
        private TextView tvDesc = null;
        private TextView tvVidMus = null;
        private TextView tvDura = null;
        private TextView tvPlayed = null;

    }

    public AttenLogAdapter(Activity context, int textViewResourceId, List<Attendance> objects){
        super(context, textViewResourceId, objects);
        this.mContext = context;
        mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View contentView, final ViewGroup parent){
        final Attendance item = getItem(position);
        final ViewHolder vh;
        if(contentView == null){
            vh = new ViewHolder();
            contentView = mInflater.inflate(R.layout.logs, parent, false);


            vh.tvTime= (TextView) contentView.findViewById(R.id.textViewDate);
            //   vh.tvSub = (TextView) contentView.findViewById(R.id.textViewSubject);
            vh.tvType = (TextView) contentView.findViewById(R.id.textViewType);
            vh.tvPage= (TextView) contentView.findViewById(R.id.textViewPage);
            vh.tvDesc= (TextView) contentView.findViewById(R.id.textViewDescription);
            // vh.tvVidMus = (TextView) contentView.findViewById(R.id.textViewVIDMUSIC);
            // vh.tvDura= (TextView) contentView.findViewById(R.id.textViewDuration);
            // vh.tvPlayed= (TextView) contentView.findViewById(R.id.textViewPlayed);


            contentView.setTag(vh);
        } else {
            vh = (ViewHolder) contentView.getTag();
        }
        vh.tvTime.setText(item.getDate());
        // vh.tvSub.setText(item.getSubject());
        // vh.tvType.setText(item.getBookType());
        vh.tvPage.setText(item.getVenue());

        //vh.tvDesc.setText();
        vh.tvDesc.setText(item.getPresent());
        // vh.tvVidMus.setText(item.getVidOrMusicName());
        // vh.tvType.setText("Page "+item.getPage());
        //vh.tvPage.setText(item.getVidOrMusicNumberOfTimesPlayed());

        return (contentView);
    }
}