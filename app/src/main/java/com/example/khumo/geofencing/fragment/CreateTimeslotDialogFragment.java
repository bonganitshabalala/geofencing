package com.example.khumo.geofencing.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.khumo.geofencing.Lecturer.LecturerHome;
import com.example.khumo.geofencing.Lecturer.MapsActivity;
import com.example.khumo.geofencing.R;
import com.example.khumo.geofencing.adapter.SubjectAdapter;
import com.example.khumo.geofencing.adapter.TimeslotAdapter;
import com.example.khumo.geofencing.pojo.Student;
import com.example.khumo.geofencing.pojo.Subject;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Bongani on 2017/10/18.
 */

public class CreateTimeslotDialogFragment extends DialogFragment  implements LocationListener{

    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 10000;
    private static String urlConne = "http://petersdana98.000webhostapp.com/getSubjectCourse.php";
    private static String urlz = "http://app-1505651429.000webhostapp.com/getContact.php";
    private static final String TAG_USER = "lec";
    private static String phoneNumber;
    private EditText etCode,etSubject;
    private String code;
    static String staffNum;


    private static String urlcon = "http://petersdana98.000webhostapp.com/getSubject.php";
    private static String urlconne = "http://petersdana98.000webhostapp.com/createSlot.php";
    private static final String TAG_SUBJECT = "subject";


    private ProgressDialog pDialog;
    static String pass;
    static String staffnumber;
    ListView listView;
    List<Subject> lectureList = new ArrayList<>();

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";

    Spinner spinDay,spinSubject;
    EditText etVenue,etLatitude,etLongitude,etStart,etEnd;
    Button btnStart,btnEnd,btnMap;
    String day,subject;

    String[] weekdays = {"Select Day","Monday","Tuesday","Wednesday","Thursday","Friday"};
    private int mHour, mMinute,mHourm, mMinutem;

    private GoogleApiClient mGoogleApiClient;
    public static final String TAG = "Activity";
    double currentLatitude , currentLongitude ;
    //double currentLatitude = -33.9791415, currentLongitude = 18.7709281;
    LocationRequest mLocationRequest;
    protected LocationListener locationListener;
    static Activity activity;

    String venue,starttime,endtime;
    static String latitude,longitude;
    static boolean valid;

    public static CreateTimeslotDialogFragment newInstance( String staff,boolean val) {

        Bundle bundle = new Bundle();
        staffNum = staff;
        valid = val;
        CreateTimeslotDialogFragment sampleFragment = new CreateTimeslotDialogFragment();
        sampleFragment.setArguments(bundle);
        return sampleFragment;
    }


    public static CreateTimeslotDialogFragment getInstance(String staff,String lat,String longi,boolean val) {

        Bundle bundle = new Bundle();
        staffNum = staff;
        latitude = lat;
        longitude = longi;
        valid = val;
        CreateTimeslotDialogFragment sampleFragment = new CreateTimeslotDialogFragment();
        sampleFragment.setArguments(bundle);
        return sampleFragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.activity_timeslot, null);
            Toolbar toolbar = (Toolbar) dialogView.findViewById(R.id.toolbar);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                toolbar.setTitle("Create timeslot");
            }
            //updateView(dialogView);
            spinDay = (Spinner) dialogView.findViewById(R.id.spinnerDay);
            spinSubject = (Spinner) dialogView.findViewById(R.id.spinnerSubject);

            btnStart = (Button) dialogView.findViewById(R.id.startTime);
            btnEnd = (Button) dialogView.findViewById(R.id.endTime);
            btnMap = (Button) dialogView.findViewById(R.id.buttonMap);

            etLatitude = (EditText) dialogView.findViewById(R.id.lati);
            etLongitude = (EditText) dialogView.findViewById(R.id.longi);
            etVenue = (EditText) dialogView.findViewById(R.id.venue);
            etStart = (EditText) dialogView.findViewById(R.id.etStart);
            etEnd = (EditText) dialogView.findViewById(R.id.etEnd);

            if(valid) {
                Log.w("TAG","Hello in am here");

              //  currentLatitude = Double.parseDouble(latitude);
              //
                //  currentLongitude = Double.parseDouble(longitude);
                Log.w("TAG"," lati "+ latitude+" sas "+ longitude);
                etLongitude.setText(longitude);
                etLatitude.setText(latitude);

            }else
            {
                Log.w("TAG","Aowa nex");
                new subjectTask(staffNum).execute();
            }



            ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(getActivity(),
                    R.layout.spinner_layout, Arrays.asList(weekdays));
            dataAdapter2.setDropDownViewResource(R.layout.spinner_layout);
            spinDay.setAdapter(dataAdapter2);

            spinDay.setOnItemSelectedListener(new CustomOnItemSelectedListeners());



            btnStart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final Calendar c = Calendar.getInstance();
                    mHour = c.get(Calendar.HOUR_OF_DAY);
                    mMinute = c.get(Calendar.MINUTE);

                    // Launch Time Picker Dialog
                    TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(),
                            new TimePickerDialog.OnTimeSetListener() {

                                @Override
                                public void onTimeSet(TimePicker view, int hourOfDay,
                                                      int minute) {

                                    etStart.setText(hourOfDay + ":" + minute);
                                }
                            }, mHour, mMinute, false);
                    timePickerDialog.show();
                }
            });

            btnEnd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final Calendar c = Calendar.getInstance();
                    mHourm = c.get(Calendar.HOUR_OF_DAY);
                    mMinutem = c.get(Calendar.MINUTE);

                    // Launch Time Picker Dialog
                    TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(),
                            new TimePickerDialog.OnTimeSetListener() {

                                @Override
                                public void onTimeSet(TimePicker view, int hourOfDay,
                                                      int minute) {



                                        etEnd.setText(hourOfDay + ":" + minute);

                                }
                            }, mHourm, mMinutem, false);
                    timePickerDialog.show();
                }
            });

            btnMap.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    etLatitude.setText("");
                    etLongitude.setText("");
                    Intent intent = new Intent(getActivity(), MapsActivity.class);
                    intent.putExtra("lat",currentLatitude);
                    intent.putExtra("longi",currentLongitude);
                    intent.putExtra("staff",staffNum);
                   startActivity(intent);
                }
            });




            int resp = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
            if (resp == ConnectionResult.SUCCESS) {

                initGoogleAPIClient();




            } else {
                Log.e(TAG, "Your Device doesn't support Google Play Services.");
                Toast.makeText(getActivity(),"Your Device doesn't support Google Play Services",Toast.LENGTH_SHORT).show();
            }

            // Create the LocationRequest object
            mLocationRequest = LocationRequest.create()
                    .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                    .setInterval(1 * 1000)        // 10 seconds, in milliseconds
                    .setFastestInterval(1 * 1000); // 1 second, in milliseconds



            //new contactTask(staffnumber,subject).execute();



            final AlertDialog alertDialog = new AlertDialog.Builder(getActivity(), R.style.MyWhiteAlertDialogStyle)
                    .setView(dialogView)
                    .setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();

                            Intent view = new Intent(getActivity(), LecturerHome.class);
                            view.putExtra("staff",staffNum);
                            startActivity(view);
                        }
                    })
                    .setPositiveButton("Create", null)
                    .create();
            alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(final DialogInterface dialog) {
                    Button b = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
                    b.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                             attemptRegistration();

                            Log.w("TAG","hshk "+ subject);


                        }
                    });
                }
            });
            return alertDialog;
        } else {
            return super.onCreateDialog(savedInstanceState);
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }

    public void initGoogleAPIClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(connectionAddListener)
                .addOnConnectionFailedListener(connectionFailedListener)
                .build();
        mGoogleApiClient.connect();
    }

    private GoogleApiClient.ConnectionCallbacks connectionAddListener =
            new GoogleApiClient.ConnectionCallbacks() {
                @Override
                public void onConnected(Bundle bundle) {
                    Log.i(TAG, "onConnected");

                    if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

                    if (location == null) {
                        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, (LocationListener) mGoogleApiClient);

                    } else {
                        //If everything went fine lets get latitude and longitude
                        currentLatitude = location.getLatitude();
                        currentLongitude = location.getLongitude();

                        Log.i(TAG, currentLatitude + " WORKS " + currentLongitude);

                        if(valid) {
                            Log.w("TAG","Hello in am here");

                            //  currentLatitude = Double.parseDouble(latitude);
                            //
                            //  currentLongitude = Double.parseDouble(longitude);
                            Log.w("TAG"," lati "+ latitude+" sas "+ longitude);
                            etLongitude.setText(longitude);
                            etLatitude.setText(latitude);

                        }else {
                            etLatitude.setText("" + currentLatitude);
                            etLongitude.setText("" + currentLongitude);
                        }



                    }


                }

                @Override
                public void onConnectionSuspended(int i) {

                    Log.e(TAG, "onConnectionSuspended");

                }
            };

    private GoogleApiClient.OnConnectionFailedListener connectionFailedListener =
            new GoogleApiClient.OnConnectionFailedListener() {
                @Override
                public void onConnectionFailed(ConnectionResult connectionResult) {
                    Log.e(TAG, "onConnectionFailed");
                }
            };


    @Override
    public void onLocationChanged(Location location) {
        currentLatitude = location.getLatitude();
        currentLongitude = location.getLongitude();
        Log.i(TAG, "onLocationChanged");
    }

    public class CustomOnItemSelectedListeners implements AdapterView.OnItemSelectedListener {

        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

            day = spinDay.getSelectedItem().toString();
            if (day.equals("Select Day")) {
                // etCity.setVisibility(View.GONE);
                // Toast.makeText(getActivity(), "Please select Bank", Toast.LENGTH_SHORT).show();
            }


        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }

    public class CustomOnItemSelectedListener implements AdapterView.OnItemSelectedListener {

        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

            Subject sub = (Subject) spinSubject.getSelectedItem();

            subject = sub.getSubject();
            Log.w("TAG","ad "+subject);


        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }

    public class subjectTask extends AsyncTask<String,String,String> {

        HttpURLConnection conn;
        URL url = null;
        String phone;

        public subjectTask(String phoneNumber) {

            phone = phoneNumber;
        }

        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Getting subjects...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... param) {

            try {

                // Enter URL address where your php file resides
                url = new URL(urlcon);

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return "exception";
            }
            try {
                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection)url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);
                conn.setDoOutput(true);

                // Append parameters to URL
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("staff", phone);
                String query = builder.build().getEncodedQuery();

                // Open connection for sending data
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                return "exception";
            }

            try {

                int response_code = conn.getResponseCode();

                Log.d("response_code",""+ response_code);
                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    Log.d("data",""+ result.toString());
                    // Pass data to onPostExecute method
                    return(result.toString());

                }else{

                    return("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }


        }

        @Override
        protected void onPostExecute(String success) {

            pDialog.dismiss();
            if (success != null){
                Log.i("Website Content", success);
                try {
                    JSONObject jsonPart = new JSONObject(success);
                    int num = jsonPart.getInt(TAG_SUCCESS);
                    JSONArray jsonArray = jsonPart.getJSONArray(TAG_SUBJECT);

                    if (num == 1) {

                        for(int x =0 ; x < jsonArray.length(); x++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(x);

                            //setAddresList(address);
                            String code = jsonObject.getString("code");
                            String subject = jsonObject.getString("subjectname");
                            Subject myContact = new Subject(code,subject);
                            if(myContact != null)
                            {
                                if (!lectureList.contains(myContact)) {
                                    lectureList.add(myContact);
                                }

                            }


                        }

                        TimeslotAdapter dataAdapter = new TimeslotAdapter(getActivity(),
                                R.layout.spinner_layout,lectureList);
                        dataAdapter.setDropDownViewResource(R.layout.spinner_layout);
                        spinSubject.setAdapter(dataAdapter);

                        spinSubject.setOnItemSelectedListener(new CustomOnItemSelectedListener());



                        //new loadAccount().execute();

                    } else {

                        //Toast.makeText(MainsActivity.this, message, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    private boolean isOnline(Context mContext) {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }


    private void attemptRegistration() {

        // Store values at the time of the login attempt.
        try {
        venue = etVenue.getText().toString();
        starttime = etStart.getText().toString();
        endtime = etEnd.getText().toString();

        latitude = etLatitude.getText().toString();
        longitude = etLongitude.getText().toString();


        boolean cancel = false;
        View focusView = null;




            day = spinDay.getSelectedItem().toString();
            if (day.equals("Select Day")) {
                cancel = true;
            }


        if (TextUtils.isEmpty(latitude)) {
            etLatitude.setError("Error - latitude can't be empty");
            focusView = etLatitude;
            cancel = true;

        }

        if (TextUtils.isEmpty(venue)) {
            etVenue.setError("Error - Venue can't be empty");
            focusView = etVenue;
            cancel = true;

        }

        if (TextUtils.isEmpty(longitude)) {
            etLongitude.setError("Error - longitude can't be empty");
            focusView = etLongitude;
            cancel = true;

        }


            if (TextUtils.isEmpty(endtime)) {
                etEnd.setHint("Click End time button to select");
                // focusView = etEnd;
                cancel = true;

            }

            if (TextUtils.isEmpty(starttime)) {
                etStart.setHint("Click Start time button to select");
                //focusView = etStart;
                cancel = true;

            }


//        if (!TextUtils.isEmpty(venue)) {
//            if(!venue.matches("[a-zA-Z]+")) {
//                etVenue.setError("Error - venue can't be empty");
//                focusView = etVenue;
//                cancel = true;
//            }
//
//        }

        if(lectureList.size() == 0)
        {
            cancel = true;
            Toast.makeText(getActivity(),"Please make sure atleast one subject is associated to lecturer",Toast.LENGTH_SHORT).show();
        }

        if(spinSubject.getSelectedItem() == null)
        {
            cancel = true;
            Toast.makeText(getActivity(),"Please make sure atleast one subject is associated to lecturer",Toast.LENGTH_SHORT).show();
        }





        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            if (!isOnline(getActivity())) {
                Toast.makeText(getActivity(), "No internet connection", Toast.LENGTH_LONG).show();
            } else {

                new createTask().execute();



            }
        }

        }catch (Exception x)
        {
            Toast.makeText(getActivity(),"Please check start time and end time if selected",Toast.LENGTH_SHORT).show();
        }
    }

    public class createTask extends AsyncTask<String,String,String> {

        HttpURLConnection conn;
        URL url = null;
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Creating timeslot...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... param) {

            try {

                // Enter URL address where your php file resides
                url = new URL(urlconne);

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return "exception";
            }
            try {
                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection)url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);
                conn.setDoOutput(true);

                // Append parameters to URL
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("subject", subject)
                        .appendQueryParameter("venue", venue)
                        .appendQueryParameter("day", day)
                        .appendQueryParameter("starttime", starttime)
                        .appendQueryParameter("endtime", endtime)
                        .appendQueryParameter("latitude", latitude)
                        .appendQueryParameter("longitude", longitude)
                        .appendQueryParameter("staffnumber", staffNum);
                String query = builder.build().getEncodedQuery();

                // Open connection for sending data
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                return "exception";
            }

            try {

                int response_code = conn.getResponseCode();

                Log.d("response_code",""+ response_code);
                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    Log.d("data",""+ result.toString());
                    // Pass data to onPostExecute method
                    return(result.toString());

                }else{

                    return("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }


        }

        @Override
        protected void onPostExecute(String success) {

            pDialog.dismiss();
            if (success != null){
                Log.i("Website Content", success);
                try {
                    JSONObject jsonPart = new JSONObject(success);
                    int num = jsonPart.getInt(TAG_SUCCESS);
                    String message = jsonPart.getString(TAG_MESSAGE);

                    if (num == 1) {


                        Intent intent = new Intent(getActivity(), LecturerHome.class); // Change to another activity
                        intent.putExtra("staff",staffNum);
                        startActivity(intent);

                    } else {
                        // pDialog.dismiss();
                        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
