package com.example.khumo.geofencing.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.khumo.geofencing.MainActivity;
import com.example.khumo.geofencing.R;
import com.example.khumo.geofencing.helper.Mail;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Pattern;



/**
 * Created by Bongani on 2017/08/24.
 */

public class ForgotDialogFragment extends DialogFragment
{
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 10000;
    private static String urlConne = "http://petersdana98.000webhostapp.com/retrievePassword.php";
    private String phone;
    private EditText etNumber;
    Activity activity;
    private ProgressDialog pDialog;
    static String data;
    String strNumber = "";

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";


    public static ForgotDialogFragment newInstance(String dat) {

        Bundle bundle = new Bundle();
        data = dat;
        ForgotDialogFragment sampleFragment =  new ForgotDialogFragment();
        sampleFragment.setArguments(bundle);
        return sampleFragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (savedInstanceState == null){
            View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_add_contact, null);
            //updateView(dialogView);
            final Bundle args = getArguments();
            Log.w("TAG","In here");

            etNumber = (EditText) dialogView.findViewById(R.id.editTextEmail);


            final AlertDialog alertDialog = new AlertDialog.Builder(getActivity(), R.style.MyWhiteAlertDialogStyle)
                    .setView(dialogView)
                    .setTitle("Reset password")
                    .setMessage("Enter email to reset password")
                    .setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .setPositiveButton("Reset", null)
                    .create();
            alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(final DialogInterface dialog) {
                    Button b = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
                    b.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            attemptRegistration();

                        }
                    });
                }
            });
            return alertDialog;
        } else {
            return super.onCreateDialog(savedInstanceState);
        }
    }

    private void attemptRegistration() {

        // Store values at the time of the login attempt.

        phone = etNumber.getText().toString();


        boolean cancel = false;
        View focusView = null;


        if (TextUtils.isEmpty(phone)) {
            etNumber.setError("Error - email can't be empty");
            focusView = etNumber;
            cancel = true;

        }

        if (!TextUtils.isEmpty(phone)) {
            if (!isEmailValid(phone)) {
                etNumber.setError("Error - Enter correct email");
                focusView = etNumber;
                cancel = true;

            }

        }



        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            if (!isOnline(getActivity())) {
                Toast.makeText(getActivity(), "No internet connection", Toast.LENGTH_LONG).show();
            } else {

                new ForgotTask().execute();
                //Toast.makeText(getActivity(),"Password sent to email",Toast.LENGTH_SHORT).show();

            }
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        boolean valid;
        String emailPattern = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        if (Pattern.matches(emailPattern, email))
        {

            valid = true;


        }else {
            valid = false;
        }

        return valid;
    }

    private boolean isOnline(Context mContext) {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    public class ForgotTask extends AsyncTask<String,String,String> {


        HttpURLConnection conn;
        URL url = null;
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Retrieving password...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... param) {

            try {

                // Enter URL address where your php file resides
                url = new URL(urlConne);

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return "exception";
            }
            try {
                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection)url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);
                conn.setDoOutput(true);

                // Append parameters to URL
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("number", phone)
                        .appendQueryParameter("choice", data);
                String query = builder.build().getEncodedQuery();

                // Open connection for sending data
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                return "exception";
            }

            try {

                int response_code = conn.getResponseCode();

                Log.d("response_code",""+ response_code);
                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    Log.d("data",""+ result.toString());
                    // Pass data to onPostExecute method
                    return(result.toString());

                }else{

                    return("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }


        }

        @Override
        protected void onPostExecute(String success) {

            pDialog.dismiss();
            if (success != null){
                Log.i("Website Content", success);
                if(!success.equals("exception")) {
                    try {
                        JSONObject jsonPart = new JSONObject(success);
                        int num = jsonPart.getInt(TAG_SUCCESS);
                        String password = jsonPart.getString(TAG_MESSAGE);
                        Log.w("TAG"," add "+ password);
                        if (num == 1) {

                            new SendMail(password).execute();
                            Intent intent = new Intent(getActivity(), MainActivity.class);
                            startActivity(intent);
                            Toast.makeText(getActivity(), "Check email for password", Toast.LENGTH_LONG).show();


                        } else {
                            Toast.makeText(getActivity(), password, Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getActivity(), "No account is associated with this email", Toast.LENGTH_LONG).show();
                    }
                }else {
                    Toast.makeText(getActivity(), "Oops Something went wrong - Connection failure", Toast.LENGTH_LONG).show();
                }
            }
        }

    }


    private class SendMail extends AsyncTask<String, Integer, Void> {

        private ProgressDialog progressDialog;
        private String pass;

        public SendMail(String password) {
            pass = password;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //  progressDialog = ProgressDialog.show(ForgotActivity.this, "Please wait", "Sending mail", true, false);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //  progressDialog.dismiss();
        }



        protected Void doInBackground(String... params) {
            Mail m = new Mail("plastingar@gmail.com", "bon123@TSHABA");

            String[] toArr = {phone};
            m.setTo(toArr);
            m.setFrom("plastingar@gmail.com");
            m.setSubject("Reset password");
            m.setBody("This is an email sent from Geofencing app.\n Your password : "+pass +"\n\n Regards "+"\n Development team");

            try {
                if(m.send()) {

                    Toast.makeText(getActivity(), "Email was sent successfully.", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getActivity(), "Email was not sent.", Toast.LENGTH_LONG).show();
                }
            } catch(Exception e) {
                Log.e("MailApp", "Could not send email", e);
            }
            return null;
        }
    }

}