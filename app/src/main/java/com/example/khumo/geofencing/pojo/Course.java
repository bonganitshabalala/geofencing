package com.example.khumo.geofencing.pojo;

/**
 * Created by Bongani on 2017/10/17.
 */

public class Course {


    private String subject;
    private String staffNumber;

    public Course() {
    }

    public Course(String subject, String staffNumber) {
        this.subject = subject;
        this.staffNumber = staffNumber;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getStaffNumber() {
        return staffNumber;
    }

    public void setStaffNumber(String staffNumber) {
        this.staffNumber = staffNumber;
    }
}
