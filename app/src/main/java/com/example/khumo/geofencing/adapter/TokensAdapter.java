package com.example.khumo.geofencing.adapter;

import android.content.Context;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.khumo.geofencing.R;
import com.example.khumo.geofencing.pojo.Timeslot;
import com.example.khumo.geofencing.pojo.Token;

import java.util.List;



/**
 * Created by Bongani on 2017/09/18.
 */

public class TokensAdapter extends RecyclerView.Adapter<TokensAdapter.MyViewHolder> {

    private Context mContext;
    private List<Timeslot> tokenList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView weekday, venue,starttime,endtime;


        public MyViewHolder(View view) {
            super(view);

            weekday = (TextView) view.findViewById(R.id.week);
            venue = (TextView) view.findViewById(R.id.venue);
            starttime = (TextView) view.findViewById(R.id.start);
            endtime = (TextView) view.findViewById(R.id.end);

        }
    }


    public TokensAdapter(Context mContext, List<Timeslot> tokenList, OnClickListener adapterClicked) {
        this.mContext = mContext;
        this.tokenList = tokenList;
        itemClickListener = adapterClicked;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.token_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Timeslot token = tokenList.get(position);
        holder.weekday.setText(token.getWeekday());
        holder.venue.setText("Venue : "+token.getVenue());
        holder.starttime.setText("Time : "+" ( "+token.getStarttime()+" - "+ token.getEndtime()+" )");
       // holder.endtime.setText(token.getEndtime());

        // loading album cover using Glide library
//        Glide.with(mContext).load(token.getThumbnail()).into(holder.thumbnail);
//
//        holder.thumbnail.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                //Toast.makeText(mContext,"Selected " + token.getPrice(),Toast.LENGTH_LONG).show();
//                if (itemClickListener != null){
//                    itemClickListener.onClick(token);
//                }
//
//
//            }
//        });
    }

//    /**
//     * Showing popup menu when tapping on 3 dots
//     */
//    private void showPopupMenu(View view) {
//        // inflate menu
//        PopupMenu popup = new PopupMenu(mContext, view);
//        MenuInflater inflater = popup.getMenuInflater();
//        inflater.inflate(R.menu.menu_tokens, popup.getMenu());
//        popup.setOnMenuItemClickListener(new MyMenuItemClickListener());
//        popup.show();
//    }
//
    private OnClickListener itemClickListener;

    public interface OnClickListener{
        void onClick(Token token);
    }
//
//    /**
//     * Click listener for popup menu items
//     */
//    class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {
//
//        public MyMenuItemClickListener() {
//        }
//
//        @Override
//        public boolean onMenuItemClick(MenuItem menuItem) {
//            switch (menuItem.getItemId()) {
//                case R.id.action_add_favourite:
//                    Toast.makeText(mContext, "Add to favourite", Toast.LENGTH_SHORT).show();
//                    return true;
//                case R.id.action_play_next:
//                    Toast.makeText(mContext, "Play next", Toast.LENGTH_SHORT).show();
//                    return true;
//                default:
//            }
//            return false;
//        }
//    }

    @Override
    public int getItemCount() {
        return tokenList.size();
    }
}
