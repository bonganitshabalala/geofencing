package com.example.khumo.geofencing;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Khumo on 9/26/2017.
 */

public class StudentTable {

    @SerializedName("studRegNo")
    public int studRegNo;

    @SerializedName("fname")
    public String fname;

    @SerializedName("lname")
    public String lname;

    @SerializedName("email")
    public String email;

    @SerializedName("idnum")
    public long idnum;

    @SerializedName("gender")
    public String gender;

    @SerializedName("studentNum")
    public String studentNum;

    @SerializedName("_password")
    public String _password;

    @SerializedName("lecRegNo")
    public int lecRegNo;
}
