package com.example.khumo.geofencing.pojo;

/**
 * Created by Bongani on 2017/09/18.
 */

public class Token {

    private String tokenName;
    private String price;
    private int thumbnail;

    public Token() {
    }

    public Token(String tokenName, String price, int thumbnail) {
        this.tokenName = tokenName;
        this.price = price;
        this.thumbnail = thumbnail;
    }

    public String getTokenName() {
        return tokenName;
    }

    public void setTokenName(String tokenName) {
        this.tokenName = tokenName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(int thumbnail) {
        this.thumbnail = thumbnail;
    }
}
