package com.example.khumo.geofencing.Lecturer;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.khumo.geofencing.BottomNavigationViewHelper;
import com.example.khumo.geofencing.R;

/**
 * Created by Khumo on 9/26/2017.
 */

public class DeleteActivity extends AppCompatActivity{

    TextView title;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete);

        title = (TextView) findViewById(R.id.tvStudentView);

        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavView_Bar);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);

        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(4);
        menuItem.setChecked(true);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId())
                {
                    case R.id.ic_arrow:
                        Intent back = new Intent(DeleteActivity.this, LecturerHome.class);
                        startActivity(back);
                        break;

                    case R.id.ic_view:
                        Intent view = new Intent(DeleteActivity.this, ViewActivity.class);
                        startActivity(view);
                        break;

                    case R.id.ic_search:
                        Intent search = new Intent(DeleteActivity.this, SearchActivity.class);
                        startActivity(search);
                        break;
//
//                    case R.id.ic_update:
//                        Intent update = new Intent(DeleteActivity.this, UpdateActivity.class);
//                        startActivity(update);
//                        break;
//
//                    case R.id.ic_delete:
//
//                        break;
                }

                return false;
            }
        });
    }
}
