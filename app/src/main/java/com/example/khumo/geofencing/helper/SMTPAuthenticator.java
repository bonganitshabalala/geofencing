package com.example.khumo.geofencing.helper;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

/**
 * Created by Bongani on 2017/04/18.
 */


public class SMTPAuthenticator extends Authenticator {
    public SMTPAuthenticator() {

        super();
    }

    @Override
    public PasswordAuthentication getPasswordAuthentication() {
        String username = "youremail@gmail";
        String password = "password";
        if ((username != null) && (username.length() > 0) && (password != null)
                && (password.length() > 0)) {

            return new PasswordAuthentication(username, password);
        }

        return null;
    }
}
