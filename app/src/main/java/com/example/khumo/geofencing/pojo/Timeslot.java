package com.example.khumo.geofencing.pojo;

/**
 * Created by Bongani on 2017/10/18.
 */

public class Timeslot {

    private String subject;
    private String venue;
    private String weekday;
    private String starttime;
    private String endtime;

    private String Latitude;
    private String Longitude;

    public Timeslot() {
    }

    public Timeslot(String subject, String venue, String weekday, String starttime, String endtime) {
        this.subject = subject;
        this.venue = venue;
        this.weekday = weekday;
        this.starttime = starttime;
        this.endtime = endtime;
    }

    public Timeslot(String venue, String weekday, String starttime, String endtime, String Latitude,String Longitude) {
        this.venue = venue;
        this.weekday = weekday;
        this.starttime = starttime;
        this.endtime = endtime;
        this.Latitude = Latitude;
        this.Longitude = Longitude;
    }


    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public String getWeekday() {
        return weekday;
    }

    public void setWeekday(String weekday) {
        this.weekday = weekday;
    }

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }
}
