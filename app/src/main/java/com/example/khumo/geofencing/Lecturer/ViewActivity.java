package com.example.khumo.geofencing.Lecturer;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.amigold.fundapter.BindDictionary;
import com.amigold.fundapter.FunDapter;
import com.amigold.fundapter.extractors.StringExtractor;
import com.example.khumo.geofencing.BottomNavigationViewHelper;
import com.example.khumo.geofencing.R;
import com.example.khumo.geofencing.StudentTable;
import com.example.khumo.geofencing.adapter.SubjectAdapter;
import com.example.khumo.geofencing.fragment.ViewStudentDialogFragment;
import com.example.khumo.geofencing.pojo.Lecture;
import com.example.khumo.geofencing.pojo.Subject;
import com.kosalgeek.android.json.JsonConverter;
import com.kosalgeek.genasync12.AsyncResponse;
import com.kosalgeek.genasync12.PostResponseAsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Khumo on 9/26/2017.
 */

public class ViewActivity extends AppCompatActivity {

    final String LOG = "ViewActivity";
    private ArrayList<StudentTable> StudenList;
    public ListView lvStudent;
    private FunDapter<StudentTable> adapter;
    TextView title;

    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 10000;
    private static String urlcon = "http://petersdana98.000webhostapp.com/getSubject.php";
    private static final String TAG_SUBJECT = "subject";
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_LECTURE = "lec";
    private ProgressDialog pDialog;
    private static String phoneNumber;
    private EditText etCode,etSubject;
    private String code,subject;
    Activity activity;

    static String staffNum;
    List<Subject> lectureList = new ArrayList<>();
    Lecture lecture;






    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);

        title = (TextView) findViewById(R.id.tvStudentView);
        lvStudent = (ListView) findViewById(R.id.lvStudent);

        staffNum = getIntent().getStringExtra("staff");
        new contactTask(staffNum).execute();

        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavView_Bar);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);

        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(1);
        menuItem.setChecked(true);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId())
                {
                    case R.id.ic_arrow:
                        Intent back = new Intent(ViewActivity.this, LecturerHome.class);
                        back.putExtra("staff",staffNum);
                        startActivity(back);
                        break;

                    case R.id.ic_view:

                        break;

                    case R.id.ic_search:
                        Intent search = new Intent(ViewActivity.this, SearchActivity.class);
                        search.putExtra("staff",staffNum);
                        startActivity(search);
                        break;

//                    case R.id.ic_update:
//                        Intent update = new Intent(ViewActivity.this, UpdateActivity.class);
//                        update.putExtra("staff",staffNum);
//                        startActivity(update);
//                        break;
//
//                    case R.id.ic_delete:
//                        Intent delete = new Intent(ViewActivity.this, DeleteActivity.class);
//                        delete.putExtra("staff",staffNum);
//                        startActivity(delete);
//                        break;
                }

                return false;
            }
        });

//        PostResponseAsyncTask taskReadStudents = new PostResponseAsyncTask(ViewActivity.this, this);
//        taskReadStudents.execute("http://petersdana98.000webhostapp.com/getSubject.php");
//
//        lvStudent = (ListView) findViewById(R.id.lvStudent);
//
//        registerForContextMenu(lvStudent);
//
        lvStudent.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {



                ViewStudentDialogFragment logOptionsDialogFragment = ViewStudentDialogFragment.newInstance(staffNum,lectureList.get(i).getSubject());
                android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.add(logOptionsDialogFragment,"newFragment");
                ft.commit();

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.lecturer_refresh_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.ic_refresh) {

            new contactTask(staffNum).execute();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

//    @Override
//    public void processFinish(String s) {
//        StudenList = new JsonConverter<StudentTable>().toArrayList(s, StudentTable.class);
//
//
//        BindDictionary<StudentTable> dict = new BindDictionary<StudentTable>();
//
//        dict.addStringField(R.id.tv_StudNum, new StringExtractor<StudentTable>() {
//            @Override
//            public String getStringValue(StudentTable studentTable, int position) {
//                return ""+ studentTable.studentNum;
//            }
//        });
//
//        dict.addStringField(R.id.tv_fname, new StringExtractor<StudentTable>() {
//            @Override
//            public String getStringValue(StudentTable studentTable, int position) {
//                return ""+ studentTable.fname;
//            }
//        });
//
//        dict.addStringField(R.id.tv_lname, new StringExtractor<StudentTable>() {
//            @Override
//            public String getStringValue(StudentTable studentTable, int position) {
//                return ""+ studentTable.lname;
//            }
//        });
//
//        dict.addStringField(R.id.tv_email, new StringExtractor<StudentTable>() {
//            @Override
//            public String getStringValue(StudentTable studentTable, int position) {
//                return ""+ studentTable.email;
//            }
//        });
//
//        adapter = new FunDapter<>(ViewActivity.this,
//                StudenList, R.layout.layout_list, dict);
//
//
//        lvStudent.setAdapter(adapter);
//    }

    public class contactTask extends AsyncTask<String,String,String> {

        HttpURLConnection conn;
        URL url = null;
        String phone;

        public contactTask(String phoneNumber) {

            phone = phoneNumber;
        }

        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(ViewActivity.this);
            pDialog.setMessage("Getting subjects...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... param) {

            try {

                // Enter URL address where your php file resides
                url = new URL(urlcon);

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return "exception";
            }
            try {
                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection)url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);
                conn.setDoOutput(true);

                // Append parameters to URL
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("staff", phone);
                String query = builder.build().getEncodedQuery();

                // Open connection for sending data
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                return "exception";
            }

            try {

                int response_code = conn.getResponseCode();

                Log.d("response_code",""+ response_code);
                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    Log.d("data",""+ result.toString());
                    // Pass data to onPostExecute method
                    return(result.toString());

                }else{

                    return("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }


        }

        @Override
        protected void onPostExecute(String success) {

            pDialog.dismiss();
            if (success != null){
                Log.i("Website Content", success);
                try {
                    JSONObject jsonPart = new JSONObject(success);
                    int num = jsonPart.getInt(TAG_SUCCESS);
                    JSONArray jsonArray = jsonPart.getJSONArray(TAG_SUBJECT);

                    if (num == 1) {

                        for(int x =0 ; x < jsonArray.length(); x++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(x);

                            //setAddresList(address);
                            String code = jsonObject.getString("code");
                            String subject = jsonObject.getString("subjectname");
                            Subject myContact = new Subject(code,subject);
                            if(myContact != null)
                            {
                                if (!lectureList.contains(myContact)) {
                                    lectureList.add(myContact);
                                }

                            }


                        }

                        SubjectAdapter subjectAdapter = new SubjectAdapter(ViewActivity.this,android.R.string.selectTextMode,lectureList);
                        lvStudent.setAdapter(subjectAdapter);



                        //new loadAccount().execute();

                    } else {

                        Toast.makeText(ViewActivity.this, "Could not load subjects", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }


}
