package com.example.khumo.geofencing.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.khumo.geofencing.R;
import com.example.khumo.geofencing.pojo.Student;

import java.util.List;

/**
 * Created by Bongani on 2017/10/17.
 */

public class StudentAdapter extends ArrayAdapter<Student> {

    private Activity mContext = null;
    private LayoutInflater mInflater = null;
    List<Student> subjects ;

    private static class ViewHolder {
        private TextView mSideBarText = null;
        private TextView mSideBar = null;
    }

    public StudentAdapter(Activity context, int textViewResourceId, List<Student> objects){
        super(context, textViewResourceId, objects);
        this.mContext = context;
        subjects = objects;
        mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public View getView(int position, View contentView, final ViewGroup parent){
        final ViewHolder vh;
        final Student subject = subjects.get(position);
        if(contentView == null){
            vh = new ViewHolder();
            contentView = mInflater.inflate(R.layout.sidebar_cart, parent, false);

            vh.mSideBarText = (TextView) contentView.findViewById(R.id.sideBarText);
            vh.mSideBar = (TextView) contentView.findViewById(R.id.sideBar);
            contentView.setTag(vh);
        } else {
            vh = (ViewHolder) contentView.getTag();
        }

        vh.mSideBar.setText(" "+ subject.getName() + "\t\t "+ subject.getSurname());
        vh.mSideBarText.setText(" "+ subject.getIdNumber());

        return (contentView);
    }


}