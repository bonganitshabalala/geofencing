package com.example.khumo.geofencing.pojo;

/**
 * Created by Bongani on 2017/10/23.
 */

public class Attendance {

    private String venue;
    private String subject;
    private String date;
    private String time;
    private String present;
    // Venue 	StudentNumber 	Date 	Time 	Present


    public Attendance(String venue, String date, String present) {
        this.venue = venue;
        this.date = date;
        this.present = present;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPresent() {
        return present;
    }

    public void setPresent(String present) {
        this.present = present;
    }
}
