package com.example.khumo.geofencing;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.kosalgeek.genasync12.AsyncResponse;
import com.kosalgeek.genasync12.PostResponseAsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.regex.Pattern;

public class LRegister extends AppCompatActivity implements View.OnClickListener,TextWatcher {

    EditText edTxtFname, edTxtLname, edTxtEmail, edTxtIDnum, edTxtStaffNum, edTxtPass, edTxtRePass;
    Button btnAddInfo, btnViewData;
    CheckBox chkBxShowPassword;
    String fname, lname, email, id, staffnum, _password, _repassword;


    private ProgressDialog pDialog;

    private static String urlCon = "http://petersdana98.000webhostapp.com/lec_register.php";

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 10000;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lregister);

        btnAddInfo = (Button) findViewById(R.id.btnAddInfo);
        edTxtFname = (EditText) findViewById(R.id.et_Fname);
        edTxtLname = (EditText) findViewById(R.id.et_Lname);
        edTxtEmail = (EditText) findViewById(R.id.et_Email);
        edTxtIDnum = (EditText) findViewById(R.id.et_IDnum);
        edTxtStaffNum = (EditText) findViewById(R.id.et_StaffNum);
        edTxtPass = (EditText) findViewById(R.id.et_Password);
        edTxtRePass = (EditText) findViewById(R.id.et_Repassword);

        edTxtFname.addTextChangedListener(this);
        edTxtLname.addTextChangedListener(this);
        edTxtRePass.addTextChangedListener(this);

        btnAddInfo.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        // getting text
        attemptRegistration();
    }

    private boolean isOnline(Context mContext) {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        boolean valid;
        String emailPattern = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        if (Pattern.matches(emailPattern, email))
        {

            valid = true;


        }else {
            valid = false;
        }

        return valid;
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic

        return password.length() > 5;
    }

    private boolean isIDValid(String number) {
        //TODO: Replace this with your own logic
        String preg = "([0-9]){2}([0-1][0-9])([0-3][0-9])([0-9]){4}([0-1])([8]){1}([0-9]){1}?";
       // String preg = "([0-9]){2}([0-1][0-9])([0-3][0-9])([0-9]){4}([0-1])([0-9]){2}?";
        boolean valid;
        if (Pattern.matches(preg, number))
        {
            if(number.length() == 13)
            {
                valid = true;
            }else
            {
                valid = false;
            }

        }else {
            valid = false;
        }





        return valid;
    }

    private void attemptRegistration() {

        // Store values at the time of the login attempt.
        fname = edTxtFname.getText().toString().trim();
        lname = edTxtLname.getText().toString().trim();
        email = edTxtEmail.getText().toString().trim();
        id = edTxtIDnum.getText().toString().trim();
        staffnum = edTxtStaffNum.getText().toString().trim();
        _password = edTxtPass.getText().toString().trim();
        _repassword = edTxtRePass.getText().toString().trim();

        boolean cancel = false;
        View focusView = null;


        if (TextUtils.isEmpty(fname)) {
            edTxtFname.setError("Error - name can't be empty");
            focusView = edTxtFname;
            cancel = true;

        }

        if (TextUtils.isEmpty(_repassword)) {
            edTxtRePass.setError("Error - confirm password can't be empty");
            focusView = edTxtRePass;
            cancel = true;

        }

        if (TextUtils.isEmpty(lname)) {
            edTxtLname.setError("Error - surname can't be empty");
            focusView = edTxtLname;
            cancel = true;

        }

        if (TextUtils.isEmpty(staffnum)) {
            edTxtStaffNum.setError("Error - staff number can't be empty");
            focusView = edTxtStaffNum;
            cancel = true;

        }
        if (!TextUtils.isEmpty(staffnum)) {
            if (staffnum.length() != 7) {
                edTxtStaffNum.setError("Error - Incorrect length for staff number length(length == 7)");
                focusView = edTxtStaffNum;
                cancel = true;

            }

        }


        if (TextUtils.isEmpty(_password)) {
            edTxtPass.setError("Error - password can't be empty");
            focusView = edTxtPass;
            cancel = true;

        }

        if (TextUtils.isEmpty(email)) {
            edTxtEmail.setError("Error - email can't be empty");
            focusView = edTxtEmail;
            cancel = true;

        }

        if (!TextUtils.isEmpty(email)) {
            if (!isEmailValid(email)) {
                edTxtEmail.setError("Error - Enter correct email");
                focusView = edTxtEmail;
                cancel = true;

            }
        }

        if (!TextUtils.isEmpty(_password)) {
            if (!isPasswordValid(_password)) {
                edTxtPass.setError("Error - password should be more than 4 digits");
                focusView = edTxtPass;
                cancel = true;

            }
        }

        if(!_password.equals(edTxtRePass.getText().toString()))
        {
            edTxtRePass.setError("Password don't match");
            focusView = edTxtRePass;
            cancel = true;
        }

        if(id != null) {
            try {
                final String year = id.substring(0, 2);
                String month = id.substring(2, 4);
                String day = id.substring(4, 6);

                int yearDate = Integer.parseInt(year);
                int monthDate = Integer.parseInt(month);
                int dayDate = Integer.parseInt(day);

                GregorianCalendar cal = new GregorianCalendar();

                int y, m, d, a;

                y = cal.get(Calendar.YEAR);
                m = cal.get(Calendar.MONTH) +1;
                d = cal.get(Calendar.DAY_OF_MONTH);
                cal.set(yearDate, monthDate, dayDate);

                a = y - cal.get(Calendar.YEAR);
                String strAge = String.valueOf(a).substring(2,String.valueOf(a).length());

                int age = Integer.parseInt(strAge);



                DateFormat dateFormat = new SimpleDateFormat("yyyy");
                Date date = new Date();

                String dateString = date != null ? dateFormat.format(date) : "N/A";

                Calendar calendar = Calendar.getInstance();
                String y1,y2,y3,y4,m1,m2,d1,d2,y3c,y4c,y3n4,y3n4c;
                y1 = "0";
                y2 = "0";


                y3c = String.valueOf(y).valueOf(String.valueOf(y).charAt(0));
                y4c = String.valueOf(y).valueOf(String.valueOf(y).charAt(1));

                y3n4c = y3c +y4c;


                int calcy3c = Integer.parseInt(y3n4c);

                int caly3n = Integer.parseInt(year);

                if(caly3n > calcy3c)
                {
                    y1 = "1";

                    y2 = "9";
                }

                if(caly3n >=0 && caly3n <= calcy3c)
                {
                    y1 = "2";

                    y2 = "0";
                }

                String yob = y1 +y2 + year;

                Log.w("HEllo","LEvels "+age+" grtg "+ yearDate + " ddf "+y+" ff "+year+ " fddf "+ yob);




                if (Integer.parseInt(yob) > y)
                {

                    focusView = edTxtIDnum;
                    cancel = true;
                    edTxtIDnum.setError("Error - Future ID number not allowed");

                }

                if ( age <= 17)
                {

                    focusView = edTxtIDnum;
                    cancel = true;
                    edTxtIDnum.setError("Error - Too young to register");

                }

//                if (age > 0)
//                {
//
//                    focusView = mID;
//                    cancel = true;
//                    mID.setError("Error - Future ID number not allowed");
//
//                }
                System.out.println("year is "+ m+" " + monthDate);
                if (monthDate == 0)
                {
                    edTxtIDnum.setError("Error - Enter correct ID number");
                    focusView = edTxtIDnum;
                    cancel = true;
                }
                if(monthDate == 2)
                {
                    if(dayDate == 30 || dayDate ==31)
                    {
                        edTxtIDnum.setError("Error - Enter correct ID number");
                        focusView = edTxtIDnum;
                        cancel = true;
                    }
                }
                System.out.println("year is "+ d+" " + dayDate);
                if (dayDate == 0)
                {
                    edTxtIDnum.setError("Error - Enter correct ID number");
                    focusView = edTxtIDnum;
                    cancel = true;
                }

//                if (!getAge(yearDate, monthDate, dayDate)) {
//                    mID.setError("Error - Enter correct ID number");
//                    focusView = mID;
//                    cancel = true;
//                }
            }catch (Exception x)
            {

            }
        }

        if (TextUtils.isEmpty(id)) {
            edTxtIDnum.setError("Error - ID number can't be empty");
            focusView = edTxtIDnum;
            cancel = true;

        }

        if (!TextUtils.isEmpty(id)) {
            if (!isIDValid(id)) {
                edTxtIDnum.setError("Enter correct ID number");
                focusView = edTxtIDnum;
                cancel = true;

            }
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            if (!isOnline(LRegister.this)) {
                Toast.makeText(LRegister.this, "No internet connection", Toast.LENGTH_LONG).show();
            } else {

                new RegistrationTask().execute();




            }
        }
    }

    public boolean getAge (int _year, int _month, int _day) {

        GregorianCalendar cal = new GregorianCalendar();

        int y, m, d, a;

        y = cal.get(Calendar.YEAR);
        m = cal.get(Calendar.MONTH) +1;
        d = cal.get(Calendar.DAY_OF_MONTH);
        cal.set(_year, _month, _day);

        a = y - cal.get(Calendar.YEAR);
        String strAge = String.valueOf(a).substring(2,String.valueOf(a).length());

        int age = Integer.parseInt(strAge);
        System.out.println("year is "+ cal.get(Calendar.YEAR) );

        boolean valid = true;

        if (age <= 17)
        {
            valid = false;
            edTxtIDnum.setError("Error - Too young to register");

        }

        if (age > 0)
        {
            valid = false;
            edTxtIDnum.setError("Error - Future ID number not allowed");

        }
        System.out.println("year is "+ m+" " + _month);
        if (_month == 0)
        {
            valid = false;
        }
        if(_month == 2)
        {
            if(_day == 30 || _day ==31)
            {
                valid = false;
            }
        }
        System.out.println("year is "+ d+" " + _day);
        if (_day == 0)
        {
            valid = false;
        }


        return valid;
    }


    public  boolean validatePhone(String phoneNo) {
        //validate phone numbers of format "1234567890"
        boolean valid ;
        String preg = "((?:\\+27|27)|0)(=99|72|82|73|83|74|84|86|79|81|71|76|60|61|62|63|64|78|)(\\d{7})";

        if (Pattern.matches(preg,phoneNo)) {

            valid = true;
        }else
        {
            valid = false;
        }
        return valid;

    }

    public  String getSex() {
        String M = "Male", F = "Female";

        int d = Integer.parseInt(id.substring(6, 7));
        if (d <= 4 || d == 0) {
            return F;
        } else {
            return M;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {

        _repassword = edTxtRePass.getText().toString();
        validateConfirm(_repassword);

        fname = edTxtFname.getText().toString();
        if(fname.contains(" "))
        {
            edTxtFname.setText(fname.replace(" ","-"));
            edTxtFname.setSelection(fname.length());
        }

        lname = edTxtLname.getText().toString();
        if(lname.contains(" "))
        {
            edTxtLname.setText(lname.replace(" ","-"));
            edTxtLname.setSelection(lname.length());
        }

    }


    public void validateConfirm(String confirm)
    {
        if(!confirm.equals(edTxtRePass.getText().toString()))
        {
            edTxtRePass.setError("Password don't match");
            edTxtRePass.requestFocus();
        }
    }

    public class RegistrationTask extends AsyncTask<String,String,String> {

        HttpURLConnection conn;
        URL url = null;
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(LRegister.this);
            pDialog.setMessage("Creating account...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... param) {

            try {

                // Enter URL address where your php file resides
                url = new URL(urlCon);

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return "exception";
            }
            try {
                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection)url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);
                conn.setDoOutput(true);

                // Append parameters to URL
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("txtFname", fname)
                        .appendQueryParameter("txtLname", lname)
                        .appendQueryParameter("txtEmail", email)
                        .appendQueryParameter("txtStaffNo", staffnum)
                        .appendQueryParameter("txtID", id)
                        .appendQueryParameter("txtPassword", _password)
                        .appendQueryParameter("mobile", "android");
                String query = builder.build().getEncodedQuery();

                // Open connection for sending data
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                return "exception";
            }

            try {

                int response_code = conn.getResponseCode();

                Log.d("response_code",""+ response_code);
                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    Log.d("data",""+ result.toString());
                    // Pass data to onPostExecute method
                    return(result.toString());

                }else{

                    return("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }


        }

        @Override
        protected void onPostExecute(String success) {

            pDialog.dismiss();
            if (success != null){
                Log.i("Website Content", success);
                try {
                    JSONObject jsonPart = new JSONObject(success);
                    int num = jsonPart.getInt(TAG_SUCCESS);
                    String message = jsonPart.getString(TAG_MESSAGE);

                    if (num == 1) {


                        Intent intent = new Intent(LRegister.this, MainActivity.class); // Change to another activity
                        startActivity(intent);
                        Toast.makeText(LRegister.this, "Registered successful", Toast.LENGTH_LONG).show();
                    } else {
                         pDialog.dismiss();
                        Toast.makeText(LRegister.this, message, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}