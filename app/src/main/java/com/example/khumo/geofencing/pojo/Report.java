package com.example.khumo.geofencing.pojo;

/**
 * Created by Bongani on 2017/11/15.
 */

public class Report {

    private String name;
    private String surname;
    private String studentNumber;
    private String subject;
    private String venue;
    private String weekday;
    private String startTime;
    private String endTime;

    public Report() {
    }

    public Report(String name, String surname, String studentNumber, String subject, String venue, String weekday, String startTime, String endTime) {
        this.name = name;
        this.surname = surname;
        this.studentNumber = studentNumber;
        this.subject = subject;
        this.venue = venue;
        this.weekday = weekday;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getStudentNumber() {
        return studentNumber;
    }

    public String getSubject() {
        return subject;
    }

    public String getVenue() {
        return venue;
    }

    public String getWeekday() {
        return weekday;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }
}
