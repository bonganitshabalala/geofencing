package com.example.khumo.geofencing.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.khumo.geofencing.R;
import com.example.khumo.geofencing.pojo.Attendance;
import com.example.khumo.geofencing.pojo.Student;

import java.util.List;

/**
 * Created by Bongani on 2017/11/02.
 */


public class StudentLogAdapter extends ArrayAdapter<Student> {

    private Activity mContext = null;
    private LayoutInflater mInflater = null;

    private static class ViewHolder {
        private TextView tvStudentnumber = null;
        private TextView tvName = null;
        private TextView tvSurname = null;


    }

    public StudentLogAdapter(Activity context, int textViewResourceId, List<Student> objects){
        super(context, textViewResourceId, objects);
        this.mContext = context;
        mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View contentView, final ViewGroup parent){
        final Student item = getItem(position);
        final ViewHolder vh;
        if(contentView == null){
            vh = new ViewHolder();
            contentView = mInflater.inflate(R.layout.logss, parent, false);


            vh.tvStudentnumber= (TextView) contentView.findViewById(R.id.studentNr);
            //   vh.tvSub = (TextView) contentView.findViewById(R.id.textViewSubject);
            vh.tvName = (TextView) contentView.findViewById(R.id.name);
            vh.tvSurname= (TextView) contentView.findViewById(R.id.surname);
           // vh.tvDesc= (TextView) contentView.findViewById(R.id.textViewDescription);
            // vh.tvVidMus = (TextView) contentView.findViewById(R.id.textViewVIDMUSIC);
            // vh.tvDura= (TextView) contentView.findViewById(R.id.textViewDuration);
            // vh.tvPlayed= (TextView) contentView.findViewById(R.id.textViewPlayed);


            contentView.setTag(vh);
        } else {
            vh = (ViewHolder) contentView.getTag();
        }
        vh.tvStudentnumber.setText(item.getStuNumber());
        // vh.tvSub.setText(item.getSubject());
        // vh.tvType.setText(item.getBookType());
        vh.tvName.setText(item.getName());

        //vh.tvDesc.setText();
        vh.tvSurname.setText(item.getSurname());
        // vh.tvVidMus.setText(item.getVidOrMusicName());
        // vh.tvType.setText("Page "+item.getPage());
        //vh.tvPage.setText(item.getVidOrMusicNumberOfTimesPlayed());

        return (contentView);
    }
}