package com.example.khumo.geofencing.Lecturer;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.khumo.geofencing.R;
import com.example.khumo.geofencing.helper.Utils;
import com.example.khumo.geofencing.adapter.AttenLogAdapter;
import com.example.khumo.geofencing.fragment.ViewStudentDialogFragment;
import com.example.khumo.geofencing.pojo.Attendance;
import com.example.khumo.geofencing.pojo.Timeslot;
import com.opencsv.CSVWriter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by Bongani on 2017/10/31.
 */

public class ViewStudentAttendance extends AppCompatActivity {

    TableLayout checkColumn;
    TableRow rowSheet;
    List<Attendance> attendanceList = new ArrayList<>();
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 10000;
    private static String urlcon = "http://petersdana98.000webhostapp.com/getAttendance.php";
    private static String urlCon = "http://petersdana98.000webhostapp.com/recordAtten.php";
    private static String urlConne = "http://petersdana98.000webhostapp.com/updateAtten.php";
    private static final String TAG_MESSAGE = "message";
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_TIMESLOT = "att";
    private ProgressDialog pDialog;
    private String staffNum;
    private String subject,studentnumber;
    int tot = 0;
    final List<String> integers = new ArrayList();
    final List<String> stringVenue = new ArrayList();
    int num = 0;

    private static String urlcone = "http://petersdana98.000webhostapp.com/getTimeslot.php";
    private static final String TAG_TIMESLOTS = "timeslot";
    List<Timeslot> timeslotList = new ArrayList<>();

    private static final String logDir = "/LogData";
    private static final String logFile = "/Attendance";
    Button btnRecord,btnUpdate;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_student_attendance);

        studentnumber = getIntent().getStringExtra("studentnumber");

        subject = getIntent().getStringExtra("subject");

        staffNum = getIntent().getStringExtra("staff");

        new contactTask(studentnumber,subject).execute();
        new timeslotTask(staffNum).execute();

         btnRecord = (Button) findViewById(R.id.buttonRecord);
         btnUpdate = (Button) findViewById(R.id.buttonUpdate);


        final Button btn = (Button) findViewById(R.id.buttonEdit);


        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ViewStudentDialogFragment logOptionsDialogFragment = ViewStudentDialogFragment.newInstance(staffNum,subject);
                android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.add(logOptionsDialogFragment,"newFragment");
                ft.commit();
//                Intent i = new Intent(ViewStudentAttendance.this,LecturerHome.class);
//                i.putExtra("staff",staffNum);
//                startActivity(i);

            }
        });


    }

    public class contactTask extends AsyncTask<String,String,String> {

        HttpURLConnection conn;
        URL url = null;
        String studentnumber,subject;

        public contactTask(String studentnumber,String subject) {

            this.studentnumber = studentnumber;
            this.subject = subject;
        }

        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(ViewStudentAttendance.this);
            pDialog.setMessage("View Attendance...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... param) {

            try {

                // Enter URL address where your php file resides
                url = new URL(urlcon);

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return "exception";
            }
            try {
                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection)url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);
                conn.setDoOutput(true);

                // Append parameters to URL
                Log.w("TAG ","s "+ studentnumber + " ssa "+ subject);
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("studentnumber", studentnumber)
                        .appendQueryParameter("subject",subject);
                String query = builder.build().getEncodedQuery();

                // Open connection for sending data
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                return "exception";
            }

            try {

                int response_code = conn.getResponseCode();

                Log.d("response_code",""+ response_code);
                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    Log.d("data",""+ result.toString());
                    // Pass data to onPostExecute method
                    return(result.toString());

                }else{

                    return("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }


        }

        @Override
        protected void onPostExecute(String success) {

            pDialog.dismiss();
            if (success != null){
                Log.i("Website Content", success);
                try {
                    JSONObject jsonPart = new JSONObject(success);
                    int num = jsonPart.getInt(TAG_SUCCESS);
                    JSONArray jsonArray = jsonPart.getJSONArray(TAG_TIMESLOT);

                    if (num == 1) {

                        for(int x =0 ; x < jsonArray.length(); x++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(x);

                            //setAddresList(address);

                            String venue = jsonObject.getString("venue");
                            String date = jsonObject.getString("date");
                            String present = jsonObject.getString("present");


                            Attendance attendance = new Attendance(venue,date,present);
                            if(attendance != null)
                            {
                                if (!attendanceList.contains(attendance)) {
                                    attendanceList.add(attendance);
                                }

                            }


                        }


                        display(attendanceList);



                        //new loadAccount().execute();

                    } else {

                        Toast.makeText(ViewStudentAttendance.this, "No reports available for this student", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }


    private TextView textView;

    public TextView makeTableRowWithText(String text, int widthInPercentOfScreenWidth, int fixedHeightInPixels) {
        int screenWidth = getResources().getDisplayMetrics().widthPixels;
        textView = new TextView(this);
        textView.setText(text);
        textView.setTextColor(Color.BLACK);
        textView.setTextSize(16);
        textView.setWidth(widthInPercentOfScreenWidth * screenWidth / 100);
        textView.setHeight(fixedHeightInPixels);
        return textView;
    }

    public TextView makeTableHeader(final String text, int widthInPercentOfScreenWidth, int fixedHeightInPixels,final List<Attendance> displayListList) {

        int screenWidth = getResources().getDisplayMetrics().widthPixels;
        final TextView textView = new TextView(this);
        textView.setText(text);
        textView.setTextColor(Color.BLUE);
        textView.setTextSize(10);
        textView.setBackgroundColor(getResources().getColor(R.color.color8));
        textView.setWidth(widthInPercentOfScreenWidth * screenWidth / 100);
        textView.setHeight(fixedHeightInPixels);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(text.equals("DATE"))
                {

                    Collections.sort(displayListList, new Comparator<Attendance>() {
                        public int compare(Attendance obj1, Attendance obj2) {
                            // ## Ascending order
                            return obj2.getDate().compareToIgnoreCase(obj1.getDate());
                            // return Integer.valueOf(obj1.getId).compareTo(obj2.getId); // To compare integer values

                        }
                    });


                }
//                if(text.equals("WEEKDAY"))
//                {   System.out.println("select name :"+text);
//                    Collections.sort(displayListList, new Comparator<Timeslot>() {
//                        public int compare(Timeslot obj1, Timeslot obj2) {
//                            // ## Ascending order
//                            return Integer.valueOf(obj2.getWeekday().compareTo(obj1.getWeekday()));
//                            // return Integer.valueOf(obj1.getId).compareTo(obj2.getId); // To compare integer values
//
//                        }
//                    });
//
//                }

                //  textToSort(textView,text);
                display(displayListList);
            }
        });

        textView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                if(text.equals("DATE")) {
                    Collections.sort(displayListList, new Comparator<Attendance>() {
                        public int compare(Attendance obj1, Attendance obj2) {
                            // ## Ascending order
                            return obj1.getDate().compareToIgnoreCase(obj2.getDate());
                            // return Integer.valueOf(obj1.empId).compareTo(obj2.empId); // To compare integer values

                            // ## Descending order
                            // return obj2.firstName.compareToIgnoreCase(obj1.firstName); // To compare string values
                            // return Integer.valueOf(obj2.empId).compareTo(obj1.empId); // To compare integer values
                        }
                    });


                }
//                else if(text.equals("WEEKDAY"))
//                {
//                    Collections.sort(displayListList, new Comparator<Timeslot>() {
//                        public int compare(Timeslot obj1, Timeslot obj2) {
//                            // ## Ascending order
//                            return Integer.valueOf(obj1.getWeekday().compareToIgnoreCase(obj2.getWeekday()));
//                            // return Integer.valueOf(obj1.getId).compareTo(obj2.getId); // To compare integer values
//
//                        }
//                    });
//                }
//                display(displayListList);
                return false;

            }
        });
        return textView;
    }

//    private void textToSort(TextView textView, String text) {
//
//        if(text.equals("SURNAME")) {
//
//            Collections.sort(displayListList, new Comparator<DisplayList>() {
//                public int compare(DisplayList obj1, DisplayList obj2) {
//                    // ## Ascending order
//                    return obj2.getPersonContent().getStrSurname().compareToIgnoreCase(obj1.getPersonContent().getStrSurname());
//                    // return Integer.valueOf(obj1.getId).compareTo(obj2.getId); // To compare integer values
//
//                }
//            });
//            display(displayListList);
//
//        }
//
//    }


    public void display(List<Attendance> displayListList) {


        if(displayListList != null)
        {

            int fixedRowHeight = 50;
            int fixedHeaderHeight = 60;

            rowSheet = new TableRow(this);
            //header (fixed vertically)
            TableRow.LayoutParams wrapWrapTableRowSheetParams = new TableRow.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT);
            int[] fixedColumnWidthsSheet = new int[]{ 25, 15, 15, 15};
            int[] scrollableColumnWidthsSheet = new int[]{ 25, 16, 15, 15};

            //header (fixed vertically)
            TableLayout header = (TableLayout) findViewById(R.id.table_header);
            header.removeAllViews();
            rowSheet.setLayoutParams(wrapWrapTableRowSheetParams);
            rowSheet.setGravity(Gravity.CENTER);
             rowSheet.addView(makeTableHeader("STUDENT NO", fixedColumnWidthsSheet[0], fixedHeaderHeight, displayListList));
            rowSheet.addView(makeTableHeader("DATE", fixedColumnWidthsSheet[1], fixedHeaderHeight, displayListList));
            rowSheet.addView(makeTableHeader("VENUE", fixedColumnWidthsSheet[2], fixedHeaderHeight, displayListList));
            rowSheet.addView(makeTableHeader("PRESENT", fixedColumnWidthsSheet[3], fixedHeaderHeight, displayListList));


            header.addView(rowSheet);


            //header for checkbox
            checkColumn = (TableLayout) findViewById(R.id.check_column);
            //header (fixed horizontally)
            TableLayout fixedColumn = (TableLayout) findViewById(R.id.fixed_column);
            fixedColumn.removeAllViews();
            //rest of the table (within a scroll view)
            TableLayout scrollablePart = (TableLayout) findViewById(R.id.scrollable_part);
            scrollablePart.removeAllViews();


            if (displayListList != null) {
                for (Attendance attendance : displayListList) {


                      TextView fixedView = makeTableRowWithText("" + studentnumber, scrollableColumnWidthsSheet[0], fixedRowHeight);
                      fixedView.setBackgroundColor(Color.WHITE);

                        fixedColumn.addView(fixedView);
                    rowSheet = new TableRow(this);
                    //rowSheet.setOnLongClickListener(get);
                    //  rowSheet.setId(person.getId());
                    rowSheet.setLayoutParams(wrapWrapTableRowSheetParams);
                    rowSheet.setGravity(Gravity.CENTER);
                    rowSheet.setBackgroundColor(Color.WHITE);
                    rowSheet.addView(makeTableRowWithText("" + attendance.getDate(), scrollableColumnWidthsSheet[1], fixedRowHeight));
                    rowSheet.addView(makeTableRowWithText("" + attendance.getVenue(), scrollableColumnWidthsSheet[2], fixedRowHeight));
                    rowSheet.addView(makeTableRowWithText("" + attendance.getPresent(), scrollableColumnWidthsSheet[2], fixedRowHeight));
//                    rowSheet.addView(makeTableRowWithText("" + timeslot.getStarttime(), scrollableColumnWidthsSheet[3], fixedRowHeight));
//                    rowSheet.addView(makeTableRowWithText("" + timeslot.getEndtime(), scrollableColumnWidthsSheet[4], fixedRowHeight));

//                rowSheet.addView(makeTableRowWithText("" + person.getStrDob(), scrollableColumnWidthsSheet[4], fixedRowHeight));
//                rowSheet.addView(makeTableRowWithText("" + person.getStrEthnic(), scrollableColumnWidthsSheet[5], fixedRowHeight));
//                rowSheet.addView(makeTableRowWithText("" + person.getStrTribal(), scrollableColumnWidthsSheet[6], fixedRowHeight));
//                rowSheet.addView(makeTableRowWithText("" + person.getStrMarital(), scrollableColumnWidthsSheet[7], fixedRowHeight));
                    scrollablePart.addView(rowSheet);

                    if(attendance.getPresent().equals("yes"))
                    {
                        tot++;
                    }


                }
               System.out.println("Hello world "+ Collections.frequency(displayListList, "Yes"));

                TextView txt = (TextView)findViewById(R.id.textViewTotal);
                txt.setText("");
                txt.setText("Attendance : "+tot + " day(s)");

                num = tot;





                btnRecord.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if(num > 0) {
                            int numdays = integers.size();
                            int numVenue = stringVenue.size();
                            Log.w("Heloo", "num" + num);

                            int totDays = numdays * 4;
                            Log.w("Heloo", "totDays" + totDays);
//                        int totNumber = totDays * 4;
//                        Log.w("Heloo","totNumber" +totNumber);
                            double perc = (num / (double) totDays) * 100;

                            DecimalFormat df = new DecimalFormat("#.00");
                            String formatPerc = df.format(perc);

                            new RecordTask(formatPerc, totDays, num).execute();


                            Log.w("Heloo", "d" + perc);
                        }else
                        {
                            Toast.makeText(ViewStudentAttendance.this,"Student didnt go to class, nothing to record",Toast.LENGTH_SHORT).show();
                        }


                    }
                });


                btnUpdate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if(num > 0) {
                            int numdays = integers.size();
                            int numVenue = stringVenue.size();
                            Log.w("Heloo", "num" + num);

                            int totDays = numdays * 4;
                            Log.w("Heloo", "totDays" + totDays);
//                        int totNumber = totDays * 4;
//                        Log.w("Heloo","totNumber" +totNumber);
                            double perc = (num / (double) totDays) * 100;

                            DecimalFormat df = new DecimalFormat("#.00");
                            String formatPerc = df.format(perc);

                            new updateTask(formatPerc, totDays, num).execute();


                            Log.w("Heloo", "d" + perc);
                        }else
                        {
                            Toast.makeText(ViewStudentAttendance.this,"Student didnt go to class, nothing to update",Toast.LENGTH_SHORT).show();
                        }


                    }
                });

            }
        }


    }


    public void makeLog() {
        try {

            CSVWriter writer;
            final ListView listView = new ListView(this);
            listView.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);


            // int tot = booksDatabaseManipulation.getContactsCount(subject);
            // Log.w("TSAG","toal "+tot);
            //  logEntries = booksDatabaseManipulation.getContact(subject);
            final AttenLogAdapter logAdapter = new AttenLogAdapter(this, R.id.textView, attendanceList);
            listView.setAdapter(logAdapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    logAdapter.notifyDataSetChanged();
                }
            });

            AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.MyWhiteAlertDialogStyle);
            builder.setTitle("Activities Log - ");
            builder.setView(listView)
                    .setNeutralButton("Store Log", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //Write Raw Frame Data to File

                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        boolean success = false;
                                        File dir = new File(Environment.getExternalStorageDirectory()+ logDir);
                                        dir.setReadable(true,false);

                                        success = (dir.mkdir() || dir.isDirectory());
                                        if (success) {
                                            String timelabel = "_" + Utils.getDateTime(new Date()).replace(" ", "_").replace(":", "h").replace("/","-");
                                            CSVWriter writer = new CSVWriter(new FileWriter(dir+ logFile + timelabel+".csv"), ',');
                                            FileOutputStream outputStream = new FileOutputStream(dir + logFile + timelabel + ".csv", false);
                                            // Runtime.getRuntime().exec("chmod 444 " + dir + logFile + timelabel + ".txt");
                                            String logString = "";
                                            // ProgressDialogStart("Application Log", "Storing app log in " + logDir + logFile + timelabel + ".txt...");
                                            for (ListIterator<Attendance> i = attendanceList.listIterator(); i.hasNext(); ) {
                                                Attendance entry = i.next();

                                              //  logString += entry.getWeekday() + ":"+ entry.getVenue()+":"+entry.getSubject();
                                               // logString += System.getProperty("line.separator");
                                                String[] entries = (studentnumber +" : " + subject + " : "+ entry.getDate() + " : "+ entry.getVenue()+" : "+entry.getPresent()).split(":");
                                                writer.writeNext(entries);
                                            }

                                            writer.close();
                                            Toast.makeText(ViewStudentAttendance.this,"Successfully created file",Toast.LENGTH_SHORT).show();


                                            // outputStream.write(logString.getBytes());
                                            // outputStream.close();

                                            //ProgressDialogClose("Log written to " + logFile + timelabel + ".txt");
                                            //mUiUtil.toast("Log written to " + logFile + timelabel + ".txt");
//
//                                            if (!rawFrames.isEmpty()){
//                                                //  ProgressDialogStart("Application Log", "Storing raw bytes log in " + logDir + rawFile + timelabel + ".txt...");
//                                                timelabel = "_" + Utils.getDateTime(new Date()).replace(" ", "_").replace(":", "h").replace("/","-");
//                                                outputStream = new FileOutputStream(dir + rawFile + timelabel + ".txt", false);
//                                                logString = "";
//
//                                                for (String raw : rawFrames) {
//                                                    logString += raw;
//                                                    logString += System.getProperty("line.separator");
//                                                }
//
//                                                outputStream.write(logString.getBytes());
//                                                outputStream.close();
//                                                //ProgressDialogClose("Raw Frames written to " + rawFile + timelabel + ".txt");
//                                                //mUiUtil.toast("Raw Frames written to " + rawFile + timelabel + ".txt");
//                                            }
                                        } else {
                                            Toast.makeText(ViewStudentAttendance.this,"Could not create directory \"/LogData\"",Toast.LENGTH_SHORT).show();
                                        }

                                        //printLog("Log written to file...");
                                    } catch (Exception e) {
                                        e.getStackTrace();
                                    }
                                }
                            }, "writeFileThread").start();

                        }
                    })
                    .setNegativeButton("Close", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (dialog != null)
                                dialog.dismiss();
                        }
                    });
            AlertDialog dialog = builder.create();
            dialog.show();
            listView.smoothScrollToPosition(logAdapter.getCount()-1);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        } catch (Exception e){
            e.getStackTrace();
        }
    }

    public class RecordTask extends AsyncTask<String,String,String> {

        HttpURLConnection conn;
        URL url = null;
        String perc,totdays,presentdays;

        public RecordTask(String formatPerc, int totDays, int num) {

            perc = formatPerc;
            totdays = String.valueOf(totDays);
            presentdays = String.valueOf(num);
        }

        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(ViewStudentAttendance.this);
            pDialog.setMessage("Recording student...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... param) {

            try {

                // Enter URL address where your php file resides
                url = new URL(urlCon);

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return "exception";
            }
            try {
                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection)url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);
                conn.setDoOutput(true);

                // Append parameters to URL
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("totaldays", totdays)
                        .appendQueryParameter("presentdays", presentdays)
                        .appendQueryParameter("percentage", perc)
                        .appendQueryParameter("staffnumber", staffNum)
                        .appendQueryParameter("studentnumber", studentnumber)
                        .appendQueryParameter("subject", subject);
                String query = builder.build().getEncodedQuery();

                // Open connection for sending data
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                return "exception";
            }

            try {

                int response_code = conn.getResponseCode();

                Log.d("response_code",""+ response_code);
                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    Log.d("data",""+ result.toString());
                    // Pass data to onPostExecute method
                    return(result.toString());

                }else{

                    return("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }


        }

        @Override
        protected void onPostExecute(String success) {

            pDialog.dismiss();
            if (success != null){
                Log.i("Website Content", success);
                try {
                    JSONObject jsonPart = new JSONObject(success);
                    int num = jsonPart.getInt(TAG_SUCCESS);
                    String message = jsonPart.getString(TAG_MESSAGE);

                    if (num == 1) {


                        Toast.makeText(ViewStudentAttendance.this, "Recorded successfully", Toast.LENGTH_LONG).show();
                       // makeLog();
                    } else {
                        // pDialog.dismiss();
                        Toast.makeText(ViewStudentAttendance.this, "Unsuccessful", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public class updateTask extends AsyncTask<String,String,String> {

        HttpURLConnection conn;
        URL url = null;
        String perc,totdays,presentdays;

        public updateTask(String formatPerc, int totDays, int num) {

            perc = formatPerc;
            totdays = String.valueOf(totDays);
            presentdays = String.valueOf(num);
        }

        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(ViewStudentAttendance.this);
            pDialog.setMessage("Updating student...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... param) {

            try {

                // Enter URL address where your php file resides
                url = new URL(urlConne);

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return "exception";
            }
            try {
                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection)url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);
                conn.setDoOutput(true);

                // Append parameters to URL
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("totaldays", totdays)
                        .appendQueryParameter("presentdays", presentdays)
                        .appendQueryParameter("percentage", perc)
                        .appendQueryParameter("staffnumber", staffNum)
                        .appendQueryParameter("studentnumber", studentnumber)
                        .appendQueryParameter("subject", subject);
                String query = builder.build().getEncodedQuery();

                // Open connection for sending data
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                return "exception";
            }

            try {

                int response_code = conn.getResponseCode();

                Log.d("response_code",""+ response_code);
                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    Log.d("data",""+ result.toString());
                    // Pass data to onPostExecute method
                    return(result.toString());

                }else{

                    return("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }


        }

        @Override
        protected void onPostExecute(String success) {

            pDialog.dismiss();
            if (success != null){
                Log.i("Website Content", success);
                try {
                    JSONObject jsonPart = new JSONObject(success);
                    int num = jsonPart.getInt(TAG_SUCCESS);
                    String message = jsonPart.getString(TAG_MESSAGE);

                    if (num == 1) {


                        Toast.makeText(ViewStudentAttendance.this, "Updated successfully", Toast.LENGTH_LONG).show();
                    } else {
                        // pDialog.dismiss();
                        Toast.makeText(ViewStudentAttendance.this, "Unsuccessful", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public class timeslotTask extends AsyncTask<String,String,String> {

        HttpURLConnection conn;
        URL url = null;
        String phone;

        public timeslotTask(String phoneNumber) {

            phone = phoneNumber;
        }

        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... param) {

            try {

                // Enter URL address where your php file resides
                url = new URL(urlcone);

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return "exception";
            }
            try {
                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection)url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);
                conn.setDoOutput(true);

                // Append parameters to URL
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("staff", phone);
                String query = builder.build().getEncodedQuery();

                // Open connection for sending data
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                return "exception";
            }

            try {

                int response_code = conn.getResponseCode();

                Log.d("response_code",""+ response_code);
                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    Log.d("data",""+ result.toString());
                    // Pass data to onPostExecute method
                    return(result.toString());

                }else{

                    return("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }


        }

        @Override
        protected void onPostExecute(String success) {

            pDialog.dismiss();
            if (success != null){
                Log.i("Website Content", success);
                try {
                    JSONObject jsonPart = new JSONObject(success);
                    int num = jsonPart.getInt(TAG_SUCCESS);
                    JSONArray jsonArray = jsonPart.getJSONArray(TAG_TIMESLOTS);

                    if (num == 1) {

                        for(int x =0 ; x < jsonArray.length(); x++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(x);

                            //setAddresList(address);
                            String subject = jsonObject.getString("subject");
                            String venue = jsonObject.getString("venue");
                            String day = jsonObject.getString("day");
                            String start = jsonObject.getString("start");
                            String end = jsonObject.getString("end");

                            Timeslot myContact = new Timeslot(subject,venue,day,start,end);
                            if(myContact != null)
                            {
                                if (!timeslotList.contains(myContact)) {
                                    timeslotList.add(myContact);
                                }

                            }


                        }

                        for(Timeslot timeslot : timeslotList)
                        {
                            if(timeslot.getSubject().equals(subject))
                            {
                                if(!integers.contains(timeslot.getWeekday())){
                                    integers.add(timeslot.getWeekday());
                                    Log.w("Heloo","d" +timeslot.getWeekday());
                                }
                                Log.w("Heloo","d" +timeslot.getWeekday());
                            }
                        }

                        for(Timeslot timeslot : timeslotList)
                        {
                            if(timeslot.getSubject().equals(subject))
                            {
                                if(!stringVenue.contains(timeslot.getVenue())){
                                    stringVenue.add(timeslot.getVenue());
                                    Log.w("Heloo","d" +timeslot.getVenue());
                                }
                                Log.w("Heloo","d" +timeslot.getVenue());
                            }
                        }
                        Log.w("Heloo","size " +integers.size());


                       //  display(timeslotList);



                        //new loadAccount().execute();

                    } else {

                       // Toast.makeText(FinishedActivity.this, "Could not load subjects", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }


}
