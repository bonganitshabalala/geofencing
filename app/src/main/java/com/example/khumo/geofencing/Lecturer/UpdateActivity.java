package com.example.khumo.geofencing.Lecturer;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.khumo.geofencing.BottomNavigationViewHelper;
import com.example.khumo.geofencing.R;

/**
 * Created by Khumo on 9/26/2017.
 */

public class UpdateActivity extends AppCompatActivity{

    TextView title;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);

        title = (TextView) findViewById(R.id.tvStudentView);

        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavView_Bar);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);

        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(3);
        menuItem.setChecked(true);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId())
                {
                    case R.id.ic_arrow:
                        Intent back = new Intent(UpdateActivity.this, LecturerHome.class);
                        startActivity(back);
                        break;

                    case R.id.ic_view:
                        Intent view = new Intent(UpdateActivity.this, ViewActivity.class);
                        startActivity(view);
                        break;

                    case R.id.ic_search:

                        break;

//                    case R.id.ic_update:
//                        Intent update = new Intent(UpdateActivity.this, UpdateActivity.class);
//                        startActivity(update);
//                        break;
//
//                    case R.id.ic_delete:
//                        Intent delete = new Intent(UpdateActivity.this, DeleteActivity.class);
//                        startActivity(delete);
//                        break;
                }

                return false;
            }
        });

    }

}
