package com.example.khumo.geofencing.Lecturer;

/**
 * Created by Bongani on 2017/11/23.
 */

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.khumo.geofencing.R;
import com.example.khumo.geofencing.fragment.CreateTimeslotDialogFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    double latitude,longitude;
    Button btnBack,btnConfirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        btnBack = (Button)findViewById(R.id.buttonBack);

        btnConfirm = (Button)findViewById(R.id.buttonConfirm);

        final String staffNumber = getIntent().getStringExtra("staff");

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.w("TAG"," lati "+ latitude+" sas "+ longitude);
                CreateTimeslotDialogFragment logOptionsDialogFragment = CreateTimeslotDialogFragment.getInstance(staffNumber,String.valueOf(latitude),String.valueOf(longitude),true);
                android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.add(logOptionsDialogFragment,"newFragment");
                ft.commit();
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CreateTimeslotDialogFragment logOptionsDialogFragment = CreateTimeslotDialogFragment.newInstance(staffNumber,false);
                android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.add(logOptionsDialogFragment,"newFragment");
                ft.commit();

            }
        });

        latitude =  getIntent().getDoubleExtra("lat",0);
        longitude =  getIntent().getDoubleExtra("longi",0);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


//        mMap.addMarker(new MarkerOptions()
//                .position(new LatLng( latitude,    longitude))
//                .rotation((float) -15.0)
//                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))
//        );
//
//        if (i == 0) {
//            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
//                    new LatLng(latitude, longitude), 7));
//            mMap.addCircle(new CircleOptions()
//                    .center(new LatLng(latitude,longitude))
//                    .radius(5000)
//                    .strokeColor(Color.RED)
//                    .fillColor(Color.RED));
//        }

        // Add a marker in Sydney, Australia, and move the camera.
        final LatLng sydney = new LatLng(latitude, longitude);
        mMap.addMarker(new MarkerOptions().position(sydney).title("You can choose a different marker"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

        CameraPosition cameraPosition = new CameraPosition.Builder().target(sydney).zoom(17.0f).build();
        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng point) {
                // TODO Auto-generated method stub
                //lstLatLngs.add(point);
                mMap.clear();
                mMap.addMarker(new MarkerOptions().position(point));

                latitude = point.latitude;
                longitude = point.longitude;
                //mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(sydney, 15));
            }
        });
    }
}

