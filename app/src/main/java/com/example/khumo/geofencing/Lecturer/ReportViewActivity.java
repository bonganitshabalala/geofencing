package com.example.khumo.geofencing.Lecturer;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.khumo.geofencing.LogAdapter;
import com.example.khumo.geofencing.R;
import com.example.khumo.geofencing.adapter.LogAllAdapter;
import com.example.khumo.geofencing.helper.FileDialog;
import com.example.khumo.geofencing.helper.Utils;
import com.example.khumo.geofencing.pojo.Report;
import com.example.khumo.geofencing.pojo.Timeslot;
import com.opencsv.CSVWriter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by Bongani on 2017/11/14.
 */


public class ReportViewActivity extends AppCompatActivity {


    TableLayout checkColumn;
    TableRow rowSheet;
    List<Report> timeslotList = new ArrayList<>();
    List<String> subjectList = new ArrayList<>();
    List<String> venueList = new ArrayList<>();
    List<String> weekList = new ArrayList<>();
    List<String> timeList = new ArrayList<>();

    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 10000;
    private static String urlcon = "http://petersdana98.000webhostapp.com/getReport.php";
    private static final String TAG_REPORT = "report";
    private static final String TAG_SUBJECT = "subject";
    private static final String TAG_SUCCESS = "success";

    private ProgressDialog pDialog;
    private String staffNum;

    private static final String logDir = "/LogData";
    private static final String logFile = "/GeoFencing";
    private Spinner spinSubject,spinVenue,spinWeekday,spinTime;

    private static final String FTYPE = ".csv";
    private static final int DIALOG_LOAD_FILE = 1000;
    File hostFile,sdcFile;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);

        staffNum = getIntent().getStringExtra("staff");

        new contactTask(staffNum).execute();

        spinSubject = (Spinner)findViewById(R.id.spinnerSubject);
        spinVenue = (Spinner)findViewById(R.id.spinnerVenue);
        spinWeekday = (Spinner)findViewById(R.id.spinnerWeek);
        spinTime = (Spinner)findViewById(R.id.spinnerTime);


        final Button btn = (Button) findViewById(R.id.buttonEdit);


        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(ReportViewActivity.this,LecturerHome.class);
                i.putExtra("staff",staffNum);
                startActivity(i);


            }
        });

        final Button btnSave = (Button) findViewById(R.id.buttonSave);


        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                makeLog();


            }
        });

        final Button btnView = (Button) findViewById(R.id.buttonView);


        btnView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                            if (Utils.isExternalStorageAvailable() || !Utils.isExternalStorageReadOnly()) {

                sdcFile = new File(Environment.getExternalStorageDirectory()+ logDir);
                if(sdcFile.exists()) {
                    FileDialog fileDialog = new FileDialog(ReportViewActivity.this, sdcFile);
                    fileDialog.addFileListener(new FileDialog.FileSelectedListener() {
                        public void fileSelected(File file) {
                            Log.d(getClass().getName(), "selected file " + file.getName());

                            Intent intent = new Intent();
                            intent.setAction(android.content.Intent.ACTION_VIEW);
                            intent.setDataAndType(Uri.fromFile(file),getMimeType(file.getAbsolutePath()));
                            startActivity(intent);

                        }
                    });
                    fileDialog.showDialog();
                }

            }


            }
        });


    }

    private String getMimeType(String url)
    {
        String parts[]=url.split("\\.");
        String extension=parts[parts.length-1];
        String type = null;
        if (extension != null) {
            MimeTypeMap mime = MimeTypeMap.getSingleton();
            type = mime.getMimeTypeFromExtension(extension);
        }
        return type;
    }


    public class contactTask extends AsyncTask<String,String,String> {

        HttpURLConnection conn;
        URL url = null;
        String phone;

        public contactTask(String phoneNumber) {

            phone = phoneNumber;
        }

        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(ReportViewActivity.this);
            pDialog.setMessage("Getting report...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... param) {

            try {

                // Enter URL address where your php file resides
                url = new URL(urlcon);

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return "exception";
            }
            try {
                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection)url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);
                conn.setDoOutput(true);

                // Append parameters to URL
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("staff", phone);
                String query = builder.build().getEncodedQuery();

                // Open connection for sending data
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                return "exception";
            }

            try {

                int response_code = conn.getResponseCode();

                Log.d("response_code",""+ response_code);
                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    Log.d("data",""+ result.toString());
                    // Pass data to onPostExecute method
                    return(result.toString());

                }else{

                    return("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }


        }

        @Override
        protected void onPostExecute(String success) {

            pDialog.dismiss();
            if (success != null){
                Log.i("Website Content", success);
                try {
                    JSONObject jsonPart = new JSONObject(success);
                    int num = jsonPart.getInt(TAG_SUCCESS);
                    JSONArray jsonArray = jsonPart.getJSONArray(TAG_REPORT);

                    if (num == 1) {

                        for(int x =0 ; x < jsonArray.length(); x++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(x);

                            //setAddresList(address);
                            String name = jsonObject.getString("name");
                            String surname = jsonObject.getString("surname");
                            String studentNumber = jsonObject.getString("student");
                            String subject = jsonObject.getString("subject");
                            String venue = jsonObject.getString("venue");
                            String day = jsonObject.getString("day");
                            String start = jsonObject.getString("start");
                            String end = jsonObject.getString("end");

                            Report report = new Report(name,surname,studentNumber,subject,venue,day,start,end);
                            if(report != null)
                            {
                                if (!timeslotList.contains(report)) {
                                    timeslotList.add(report);
                                }

                                if (!subjectList.contains(report.getSubject())) {
                                    subjectList.add(report.getSubject());
                                }

                                if (!weekList.contains(report.getWeekday())) {
                                    weekList.add(report.getWeekday());
                                }

                                if (!venueList.contains(report.getVenue())) {
                                    venueList.add(report.getVenue());
                                }

                                if (!timeList.contains(report.getStartTime())) {
                                    timeList.add(report.getStartTime());
                                }

                            }


                        }




                        ArrayAdapter dataAdapter = new ArrayAdapter(ReportViewActivity.this,
                                R.layout.spinner_report_view,subjectList);
                        dataAdapter.setDropDownViewResource(R.layout.spinner_layout);
                        spinSubject.setAdapter(dataAdapter);

                        spinSubject.setOnItemSelectedListener(new CustomOnItemSelectedListener());

                        //------------------------------------------------------------------------
                        ArrayAdapter dataAdapter1 = new ArrayAdapter(ReportViewActivity.this,
                                R.layout.spinner_report_view,venueList);
                        dataAdapter1.setDropDownViewResource(R.layout.spinner_layout);
                        spinVenue.setAdapter(dataAdapter1);

                        spinVenue.setOnItemSelectedListener(new CustomOnItemSelectedListenerr());
                        //-------------------------------------------------------------------------
                        ArrayAdapter dataAdapter2 = new ArrayAdapter(ReportViewActivity.this,
                                R.layout.spinner_report_view,weekList);
                        dataAdapter2.setDropDownViewResource(R.layout.spinner_layout);
                        spinWeekday.setAdapter(dataAdapter2);

                        spinWeekday.setOnItemSelectedListener(new CustomOnItemSelectedListenerrr());

                        ArrayAdapter dataAdapter3 = new ArrayAdapter(ReportViewActivity.this,
                                R.layout.spinner_report_view,timeList);
                        dataAdapter3.setDropDownViewResource(R.layout.spinner_layout);
                        spinTime.setAdapter(dataAdapter3);

                        spinTime.setOnItemSelectedListener(new CustomOnItemSelectedListenerrrr());

                        display(timeslotList);



                        //new loadAccount().execute();

                    } else {

                        Toast.makeText(ReportViewActivity.this, "Could not retrieve report", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    private TextView textView;

    public TextView makeTableRowWithText(String text, int widthInPercentOfScreenWidth, int fixedHeightInPixels) {
        int screenWidth = getResources().getDisplayMetrics().widthPixels;
        textView = new TextView(this);
        textView.setText(text);
        textView.setTextColor(Color.BLACK);
        textView.setTextSize(16);
        textView.setWidth(widthInPercentOfScreenWidth * screenWidth / 100);
        textView.setHeight(fixedHeightInPixels);
        return textView;
    }

    public TextView makeTableHeader(final String text, int widthInPercentOfScreenWidth, int fixedHeightInPixels,final List<Report> displayListList) {

        int screenWidth = getResources().getDisplayMetrics().widthPixels;
        final TextView textView = new TextView(this);
        textView.setText(text);
        textView.setTextColor(Color.BLUE);
        textView.setTextSize(10);
        textView.setBackgroundColor(getResources().getColor(R.color.color8));
        textView.setWidth(widthInPercentOfScreenWidth * screenWidth / 100);
        textView.setHeight(fixedHeightInPixels);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(text.equals("NAME"))
                {

                    Collections.sort(displayListList, new Comparator<Report>() {
                        public int compare(Report obj1, Report obj2) {
                            // ## Ascending order
                            return obj2.getName().compareToIgnoreCase(obj1.getName());
                            // return Integer.valueOf(obj1.getId).compareTo(obj2.getId); // To compare integer values

                        }
                    });


                }
                if(text.equals("SURNAME"))
                {   System.out.println("select name :"+text);
                    Collections.sort(displayListList, new Comparator<Report>() {
                        public int compare(Report obj1, Report obj2) {
                            // ## Ascending order
                            return Integer.valueOf(obj2.getSurname().compareTo(obj1.getSurname()));
                            // return Integer.valueOf(obj1.getId).compareTo(obj2.getId); // To compare integer values

                        }
                    });

                }

                //  textToSort(textView,text);
                display(displayListList);
            }
        });

        textView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                if(text.equals("NAME")) {
                    Collections.sort(displayListList, new Comparator<Report>() {
                        public int compare(Report obj1, Report obj2) {
                            // ## Ascending order
                            return obj1.getName().compareToIgnoreCase(obj2.getName());
                            // return Integer.valueOf(obj1.empId).compareTo(obj2.empId); // To compare integer values

                            // ## Descending order
                            // return obj2.firstName.compareToIgnoreCase(obj1.firstName); // To compare string values
                            // return Integer.valueOf(obj2.empId).compareTo(obj1.empId); // To compare integer values
                        }
                    });


                }else if(text.equals("SURNAME"))
                {
                    Collections.sort(displayListList, new Comparator<Report>() {
                        public int compare(Report obj1, Report obj2) {
                            // ## Ascending order
                            return Integer.valueOf(obj1.getSurname().compareToIgnoreCase(obj2.getSurname()));
                            // return Integer.valueOf(obj1.getId).compareTo(obj2.getId); // To compare integer values

                        }
                    });
                }
                display(displayListList);
                return false;

            }
        });
        return textView;
    }

//    private void textToSort(TextView textView, String text) {
//
//        if(text.equals("SURNAME")) {
//
//            Collections.sort(displayListList, new Comparator<DisplayList>() {
//                public int compare(DisplayList obj1, DisplayList obj2) {
//                    // ## Ascending order
//                    return obj2.getPersonContent().getStrSurname().compareToIgnoreCase(obj1.getPersonContent().getStrSurname());
//                    // return Integer.valueOf(obj1.getId).compareTo(obj2.getId); // To compare integer values
//
//                }
//            });
//            display(displayListList);
//
//        }
//
//    }


    public void display(List<Report> displayListList) {

        if(displayListList != null)
        {

            int fixedRowHeight = 50;
            int fixedHeaderHeight = 60;

            rowSheet = new TableRow(this);
            //header (fixed vertically)
            TableRow.LayoutParams wrapWrapTableRowSheetParams = new TableRow.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT);
            int[] fixedColumnWidthsSheet = new int[]{12, 10, 15, 23, 15, 15, 10, 10};
            int[] scrollableColumnWidthsSheet = new int[]{12, 10, 15, 25, 10, 15, 10, 10};

            //header (fixed vertically)
            TableLayout header = (TableLayout) findViewById(R.id.table_header);
            header.removeAllViews();
            rowSheet.setLayoutParams(wrapWrapTableRowSheetParams);
            rowSheet.setGravity(Gravity.START);

            rowSheet.addView(makeTableHeader("STUDENT NO.", fixedColumnWidthsSheet[0], fixedHeaderHeight, displayListList));
            rowSheet.addView(makeTableHeader("NAME", fixedColumnWidthsSheet[1], fixedHeaderHeight, displayListList));
            rowSheet.addView(makeTableHeader("SURNAME", fixedColumnWidthsSheet[2], fixedHeaderHeight, displayListList));
            rowSheet.addView(makeTableHeader("SUBJECT", fixedColumnWidthsSheet[3], fixedHeaderHeight, displayListList));
            rowSheet.addView(makeTableHeader("VENUE", fixedColumnWidthsSheet[4], fixedHeaderHeight, displayListList));
            rowSheet.addView(makeTableHeader("WEEKDAY", fixedColumnWidthsSheet[5], fixedHeaderHeight, displayListList));
            rowSheet.addView(makeTableHeader("START TIME", fixedColumnWidthsSheet[6], fixedHeaderHeight, displayListList));
            rowSheet.addView(makeTableHeader("END TIME", fixedColumnWidthsSheet[7], fixedHeaderHeight, displayListList));

            header.addView(rowSheet);


            //header for checkbox
            checkColumn = (TableLayout) findViewById(R.id.check_column);
            //header (fixed horizontally)
            TableLayout fixedColumn = (TableLayout) findViewById(R.id.fixed_column);
            fixedColumn.removeAllViews();
            //rest of the table (within a scroll view)
            TableLayout scrollablePart = (TableLayout) findViewById(R.id.scrollable_part);
            scrollablePart.removeAllViews();

            if (displayListList != null) {
                for (Report report : displayListList) {


                      TextView fixedView = makeTableRowWithText("" + report.getStudentNumber(), scrollableColumnWidthsSheet[0], fixedRowHeight);
                      fixedView.setBackgroundColor(Color.WHITE);

                        fixedColumn.addView(fixedView);
                    rowSheet = new TableRow(this);
                    //rowSheet.setOnLongClickListener(get);
                    //  rowSheet.setId(person.getId());
                    rowSheet.setLayoutParams(wrapWrapTableRowSheetParams);
                    rowSheet.setGravity(Gravity.CENTER);
                    rowSheet.setBackgroundColor(Color.WHITE);
                    rowSheet.addView(makeTableRowWithText("" + report.getName(), scrollableColumnWidthsSheet[1], fixedRowHeight));
                    rowSheet.addView(makeTableRowWithText("" + report.getSurname(), scrollableColumnWidthsSheet[2], fixedRowHeight));
                    rowSheet.addView(makeTableRowWithText("" + report.getSubject(), scrollableColumnWidthsSheet[3], fixedRowHeight));
                    rowSheet.addView(makeTableRowWithText("" + report.getVenue(), scrollableColumnWidthsSheet[4], fixedRowHeight));
                    rowSheet.addView(makeTableRowWithText("" + report.getWeekday(), scrollableColumnWidthsSheet[5], fixedRowHeight));
                    rowSheet.addView(makeTableRowWithText("" + report.getStartTime(), scrollableColumnWidthsSheet[6], fixedRowHeight));
                    rowSheet.addView(makeTableRowWithText("" + report.getEndTime(), scrollableColumnWidthsSheet[7], fixedRowHeight));

//                rowSheet.addView(makeTableRowWithText("" + person.getStrDob(), scrollableColumnWidthsSheet[4], fixedRowHeight));
//                rowSheet.addView(makeTableRowWithText("" + person.getStrEthnic(), scrollableColumnWidthsSheet[5], fixedRowHeight));
//                rowSheet.addView(makeTableRowWithText("" + person.getStrTribal(), scrollableColumnWidthsSheet[6], fixedRowHeight));
//                rowSheet.addView(makeTableRowWithText("" + person.getStrMarital(), scrollableColumnWidthsSheet[7], fixedRowHeight));
                    scrollablePart.addView(rowSheet);


                }

                rawFrames = displayListList;
                //makeLog();
            }

        }


    }

    List<Report> rawFrames = new ArrayList();
    public void makeLog() {
        try {

            CSVWriter writer;
            final ListView listView = new ListView(this);
            listView.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);


            // int tot = booksDatabaseManipulation.getContactsCount(subject);
            // Log.w("TSAG","toal "+tot);
            //  logEntries = booksDatabaseManipulation.getContact(subject);
            final LogAllAdapter logAdapter = new LogAllAdapter(ReportViewActivity.this, R.id.textView, rawFrames);
            listView.setAdapter(logAdapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    logAdapter.notifyDataSetChanged();
                }
            });

            AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.MyWhiteAlertDialogStyle);
            builder.setTitle("Activities Log - ");
            builder.setView(listView)
                    .setNeutralButton("Store Log", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //Write Raw Frame Data to File

                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        boolean success = false;
                                        File dir = new File(Environment.getExternalStorageDirectory()+ logDir);
                                        dir.setReadable(true,false);

                                        success = (dir.mkdir() || dir.isDirectory());
                                        if (success) {
                                            String timelabel = "_" + Utils.getDateTime(new Date()).replace(" ", "_").replace(":", "h").replace("/","-");
                                            CSVWriter writer = new CSVWriter(new FileWriter(dir+ logFile + timelabel+".csv"), ',');
                                            FileOutputStream outputStream = new FileOutputStream(dir + logFile + timelabel + ".csv", false);
                                            // Runtime.getRuntime().exec("chmod 444 " + dir + logFile + timelabel + ".txt");
                                            String logString = "";
                                            // ProgressDialogStart("Application Log", "Storing app log in " + logDir + logFile + timelabel + ".txt...");
                                            for (ListIterator<Report> i = rawFrames.listIterator(); i.hasNext(); ) {
                                                Report entry = i.next();

                                                logString += entry.getWeekday() + ":"+ entry.getVenue()+":"+entry.getSubject();
                                                logString += System.getProperty("line.separator");
                                                String[] entries = (entry.getStudentNumber()+" : "+ entry.getName()+" : "+ entry.getStudentNumber()+" : "+ entry.getWeekday() + " : "+ entry.getVenue()+" : "+entry.getSubject()).split(":");
                                                writer.writeNext(entries);
                                            }

                                            writer.close();
                                            Toast.makeText(ReportViewActivity.this,"Successfully created file",Toast.LENGTH_SHORT).show();


                                        } else {
                                            Toast.makeText(ReportViewActivity.this,"Could not create directory \"/LogData\"",Toast.LENGTH_SHORT).show();
                                        }

                                        //printLog("Log written to file...");
                                    } catch (Exception e) {
                                        e.getStackTrace();
                                    }
                                }
                            }, "writeFileThread").start();

                        }
                    })
                    .setNegativeButton("Close", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (dialog != null)
                                dialog.dismiss();
                        }
                    });
            AlertDialog dialog = builder.create();
            dialog.show();
            listView.smoothScrollToPosition(logAdapter.getCount()-1);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        } catch (Exception e){
            e.getStackTrace();
        }
    }

    String subject,venue,weekday,time;
    public class CustomOnItemSelectedListener implements AdapterView.OnItemSelectedListener {

        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

            subject =  spinSubject.getSelectedItem().toString();

            List<Report> result = new ArrayList<>();

            for (Report person: timeslotList) {
                if (person.getSubject().equals(subject)) {
                    result.add(person);
                }
            }

//            venue =  spinVenue.getSelectedItem().toString();
//
//
//            for (Report person: timeslotList) {
//                if (person.getVenue().equals(venue)) {
//                    result.add(person);
//                }
//            }
//
//            weekday =  spinWeekday.getSelectedItem().toString();
//
//           // List<Report> result = new ArrayList<>();
//
//            for (Report person: timeslotList) {
//                if (person.getWeekday().equals(weekday)) {
//                    result.add(person);
//                }
//            }

            System.out.println(result);

            display(result);


        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }

    public class CustomOnItemSelectedListenerr implements AdapterView.OnItemSelectedListener {

        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {


            venue =  spinVenue.getSelectedItem().toString();





            Log.w("TAG","ad "+subject);

            List<Report> result = new ArrayList<>();

            for (Report person: timeslotList) {
                if (person.getVenue().equals(venue)) {
                    result.add(person);
                }
            }

//            subject =  spinSubject.getSelectedItem().toString();
//
//          //  List<Report> result = new ArrayList<>();
//
//            for (Report person: timeslotList) {
//                if (person.getSubject().equals(subject)) {
//                    result.add(person);
//                }
//            }
//
//            weekday =  spinWeekday.getSelectedItem().toString();
//
//          //  List<Report> result = new ArrayList<>();
//
//            for (Report person: timeslotList) {
//                if (person.getWeekday().equals(weekday)) {
//                    result.add(person);
//                }
//            }


            System.out.println(result);

            display(result);


        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }

    public class CustomOnItemSelectedListenerrr implements AdapterView.OnItemSelectedListener {

        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {


            weekday =  spinWeekday.getSelectedItem().toString();




            Log.w("TAG","ad "+subject);

            List<Report> result = new ArrayList<>();


            for (Report person: timeslotList) {
                if (person.getWeekday().equals(weekday)) {
                    result.add(person);
                }
            }

//            subject =  spinSubject.getSelectedItem().toString();
//
//            //List<Report> result = new ArrayList<>();
//
//            for (Report person: timeslotList) {
//                if (person.getSubject().equals(subject)) {
//                    result.add(person);
//                }
//            }
//
//            venue =  spinVenue.getSelectedItem().toString();
//
//          //  List<Report> result = new ArrayList<>();
//
//            for (Report person: timeslotList) {
//                if (person.getVenue().equals(venue)) {
//                    result.add(person);
//                }
//            }
//
//            System.out.println(result);

            display(result);


        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }

    public class CustomOnItemSelectedListenerrrr implements AdapterView.OnItemSelectedListener {

        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {


            time =  spinTime.getSelectedItem().toString();




            Log.w("TAG","ad "+subject);

            List<Report> result = new ArrayList<>();


            for (Report person: timeslotList) {
                if (person.getStartTime().equals(time)) {
                    result.add(person);
                }
            }

//            subject =  spinSubject.getSelectedItem().toString();
//
//            //List<Report> result = new ArrayList<>();
//
//            for (Report person: timeslotList) {
//                if (person.getSubject().equals(subject)) {
//                    result.add(person);
//                }
//            }
//
//            venue =  spinVenue.getSelectedItem().toString();
//
//          //  List<Report> result = new ArrayList<>();
//
//            for (Report person: timeslotList) {
//                if (person.getVenue().equals(venue)) {
//                    result.add(person);
//                }
//            }
//
//            System.out.println(result);

            display(result);


        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }

}