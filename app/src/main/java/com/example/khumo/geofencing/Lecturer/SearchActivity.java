package com.example.khumo.geofencing.Lecturer;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.amigold.fundapter.BindDictionary;
import com.amigold.fundapter.FunDapter;
import com.amigold.fundapter.extractors.StringExtractor;
import com.example.khumo.geofencing.BottomNavigationViewHelper;
import com.example.khumo.geofencing.R;
import com.example.khumo.geofencing.Student.EditStudentProfileActivity;
import com.example.khumo.geofencing.StudentTable;
import com.example.khumo.geofencing.adapter.SearchAdapter;
import com.example.khumo.geofencing.adapter.StudentAdapter;
import com.example.khumo.geofencing.pojo.Student;
import com.example.khumo.geofencing.pojo.Subject;
import com.kosalgeek.android.json.JsonConverter;
import com.kosalgeek.genasync12.AsyncResponse;
import com.kosalgeek.genasync12.PostResponseAsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Khumo on 9/26/2017.
 */

public class SearchActivity extends AppCompatActivity {

    TextView title;
    Button btnSearch;
    EditText etStudentNo;
    String staffNumber;

    private List<Student> StudenList = new ArrayList<>();
    public ListView lvStudent;
    private ProgressDialog pDialog;
    private static String urlz = "http://petersdana98.000webhostapp.com/searchStudent.php";
    private static String urlCon = "http://petersdana98.000webhostapp.com/updateStudent.php";
    private static String urlcon = "http://petersdana98.000webhostapp.com/getCourseSubject.php";


    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";
    private static final String TAG_USER = "lec";
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 10000;
    SearchAdapter subjectAdapter;
    private Spinner spinSubject;
    private static final String TAG_SUBJECT = "course";
    List<String> subjectList = new ArrayList<>();
    String studentNum = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        staffNumber = getIntent().getStringExtra("staff");



        title = (TextView) findViewById(R.id.tvStudentView);
        etStudentNo = (EditText)findViewById(R.id.etStudentNo);

        spinSubject = (Spinner)findViewById(R.id.spinner);

        lvStudent = (ListView)findViewById(R.id.lvResults);
        subjectAdapter = new SearchAdapter(SearchActivity.this,android.R.string.selectTextMode,StudenList);
        lvStudent.setAdapter(subjectAdapter);



        btnSearch = (Button)findViewById(R.id.btnSearch);
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                subjectAdapter.clear();
                subjectAdapter.notifyDataSetChanged();

                if(!etStudentNo.getText().toString().equals(""))
                {

                    new statusTask(staffNumber,etStudentNo.getText().toString()).execute();
                }
            }
        });






        lvStudent.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                if(spinSubject.getSelectedItem() != null) {
                    Intent intent = new Intent(SearchActivity.this, ViewStudentAttendance.class);
                    intent.putExtra("studentnumber", studentNum);
                    intent.putExtra("subject", subject);
                    intent.putExtra("staff", staffNumber);
                    startActivity(intent);
                }else
                {
                    Toast.makeText(SearchActivity.this,"Select subject",Toast.LENGTH_SHORT).show();
                }

//                Intent intent = new Intent(SearchActivity.this, ViewActivity.class);
//                intent.putExtra("staff",staffNumber);
//                startActivity(intent);

            }
        });

        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavView_Bar);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);

        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(2);
        menuItem.setChecked(true);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId())
                {
                    case R.id.ic_arrow:
                        Intent back = new Intent(SearchActivity.this, LecturerHome.class);
                        back.putExtra("staff",staffNumber);
                        startActivity(back);
                        break;

                    case R.id.ic_view:
                        Intent view = new Intent(SearchActivity.this, ViewActivity.class);
                        view.putExtra("staff",staffNumber);
                        startActivity(view);
                        break;

                    case R.id.ic_search:

                        break;

//                    case R.id.ic_update:
//                        Intent update = new Intent(SearchActivity.this, UpdateActivity.class);
//                        update.putExtra("staff",staffNumber);
//                        startActivity(update);
//                        break;
//
//                    case R.id.ic_delete:
//                        Intent delete = new Intent(SearchActivity.this, DeleteActivity.class);
//                        delete.putExtra("staff",staffNumber);
//                        startActivity(delete);
//                        break;
                }

                return false;
            }
        });
    }

    public class statusTask extends AsyncTask<String,String,String> {

        HttpURLConnection conn;
        URL url = null;
        String staffNum,studNum;

        public statusTask(String emails, String s) {
            staffNum = emails;
            studNum = s;
        }

        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(SearchActivity.this);
            pDialog.setMessage("Getting account...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... param) {

            try {

                // Enter URL address where your php file resides
                url = new URL(urlz);

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return "exception";
            }
            try {
                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection)url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);
                conn.setDoOutput(true);

                // Append parameters to URL
                Log.i("HE "," d"+ staffNum+" sd "+ studNum);
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("staff", staffNum)
                        .appendQueryParameter("studentnumber", studNum);
                String query = builder.build().getEncodedQuery();

                // Open connection for sending data
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                return "exception";
            }

            try {

                int response_code = conn.getResponseCode();

                Log.d("response_code",""+ response_code);
                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    Log.d("data",""+ result.toString());
                    // Pass data to onPostExecute method
                    return(result.toString());

                }else{

                    return("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }


        }

        @Override
        protected void onPostExecute(String success) {

            pDialog.dismiss();
            if (success != null){
                Log.i("Website Content", success);
                try {
                    JSONObject jsonPart = new JSONObject(success);
                    int num = jsonPart.getInt(TAG_SUCCESS);
                    JSONArray jsonArray = jsonPart.getJSONArray(TAG_USER);

                    if (num == 1) {

                        for(int x =0 ; x < jsonArray.length(); x++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(x);

                            //setAddresList(address);
                            studentNum = jsonObject.getString("studentnumber");
                            String name = jsonObject.getString("name");
                            String surname = jsonObject.getString("surname");
                            String email = jsonObject.getString("email");
                            String idnumber = jsonObject.getString("idnumber");
                            String gender = jsonObject.getString("gender");

                            Student student = new Student(studentNum,name,surname,email,idnumber,gender);
                            if(student != null)
                            {
                                StudenList.add(student);
                                etStudentNo.setText("");

                            }


                        }

                        new subjectTask(studentNum).execute();




                        if(StudenList.size() == 0){
                            Toast.makeText(SearchActivity.this, "Could not find student number", Toast.LENGTH_LONG).show();

                        }else{
                            subjectAdapter = new SearchAdapter(SearchActivity.this,android.R.string.selectTextMode,StudenList);
                            lvStudent.setAdapter(subjectAdapter);
                        }

                        //new loadAccount().execute();

                    } else {

                        Toast.makeText(SearchActivity.this, "Could not find student number", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }


    public class subjectTask extends AsyncTask<String,String,String> {

        HttpURLConnection conn;
        URL url = null;
        String phone;

        public subjectTask(String phoneNumber) {

            phone = phoneNumber;
        }

        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(SearchActivity.this);
            pDialog.setMessage("Getting subjects...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... param) {

            try {

                // Enter URL address where your php file resides
                url = new URL(urlcon);

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return "exception";
            }
            try {
                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection)url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);
                conn.setDoOutput(true);

                // Append parameters to URL
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("studentnumber", phone);
                String query = builder.build().getEncodedQuery();

                // Open connection for sending data
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                return "exception";
            }

            try {

                int response_code = conn.getResponseCode();

                Log.d("response_code",""+ response_code);
                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    Log.d("data",""+ result.toString());
                    // Pass data to onPostExecute method
                    return(result.toString());

                }else{

                    return("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }


        }

        @Override
        protected void onPostExecute(String success) {

            pDialog.dismiss();
            if (success != null){
                Log.i("Website Content", success);
                try {
                    JSONObject jsonPart = new JSONObject(success);
                    int num = jsonPart.getInt(TAG_SUCCESS);
                    JSONArray jsonArray = jsonPart.getJSONArray(TAG_SUBJECT);

                    if (num == 1) {

                        for(int x =0 ; x < jsonArray.length(); x++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(x);

                            //setAddresList(address);
                            String subject = jsonObject.getString("subject");
                            String staff = jsonObject.getString("staff");

                            if(subject != null)
                            {
                                if (!subjectList.contains(subject)) {
                                    subjectList.add(subject);
                                }

                            }


                        }

                        ArrayAdapter dataAdapter = new ArrayAdapter(SearchActivity.this,
                                R.layout.spinner_report_view,subjectList);
                        dataAdapter.setDropDownViewResource(R.layout.spinner_layout);
                        spinSubject.setAdapter(dataAdapter);
                        spinSubject.performClick();

                        spinSubject.setOnItemSelectedListener(new CustomOnItemSelectedListenerrr());


                    } else {

                        Toast.makeText(SearchActivity.this, "Could not load subjects", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    String subject;

    public class CustomOnItemSelectedListenerrr implements AdapterView.OnItemSelectedListener {

        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {


            subject =  spinSubject.getSelectedItem().toString();



        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }


}
