package com.example.khumo.geofencing;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.khumo.geofencing.Lecturer.LecturerHome;
import com.example.khumo.geofencing.Student.StudentHome;
import com.example.khumo.geofencing.fragment.ForgotDialogFragment;
import com.kosalgeek.genasync12.AsyncResponse;
import com.kosalgeek.genasync12.PostResponseAsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText etLoginNo, etPassword;
    RadioButton rdBtnLecturer, rdBtnStudent;
    Button btnLogin;
    TextView link_signup;
    private ProgressDialog pDialog;
    private TextView txtForgot;

    private static String urlCon = "http://petersdana98.000webhostapp.com/lec_login.php";
    private static String urlConne = "http://petersdana98.000webhostapp.com/stud_login.php";


    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 10000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActivityCompat.requestPermissions(MainActivity.this,
                new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE},
                1);

        etLoginNo = (EditText) findViewById(R.id.etLoginNo);
        etPassword = (EditText) findViewById(R.id.etPassword);
        rdBtnLecturer = (RadioButton) findViewById(R.id.rdBtnLecturer);
        rdBtnStudent = (RadioButton) findViewById(R.id.rdBtnStudent);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        link_signup = (TextView) findViewById(R.id.link_signup);
        txtForgot = (TextView) findViewById(R.id.textViewForgot);
        txtForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (rdBtnLecturer.isChecked()) {
                    ForgotDialogFragment logOptionsDialogFragment = ForgotDialogFragment.newInstance("staff");
                    android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    ft.add(logOptionsDialogFragment, "newFragment");
                    ft.commit();
                }else
                {
                    ForgotDialogFragment logOptionsDialogFragment = ForgotDialogFragment.newInstance("student");
                    android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    ft.add(logOptionsDialogFragment, "newFragment");
                    ft.commit();
                }
            }
        });

        // Button Click
        btnLogin.setOnClickListener(this);
        setLink_signup();
    }

    @Override
    public void onClick(View view) {

        // Getting data from the edit-text
        String loginNo = etLoginNo.getText().toString().trim();
        String password = etPassword.getText().toString();
        if (!TextUtils.isEmpty(loginNo) && !TextUtils.isEmpty(password)) {
            if (rdBtnLecturer.isChecked()) {
                etLoginNo.setHint("Staff number");
                if (loginNo.length() == 7) {
                    if (password.length() >= 6) {
                        new loginTask("lec",loginNo,password).execute();
                    } else {
                        Toast.makeText(MainActivity.this, "Password is too short", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(MainActivity.this, "Staff number length should be equal to 7", Toast.LENGTH_LONG).show();
                }
            }
        } else {
            Toast.makeText(MainActivity.this, "Fill all the fields", Toast.LENGTH_LONG).show();
        }

        /*
        Student
         */

        if (!TextUtils.isEmpty(loginNo) && !TextUtils.isEmpty(password)) {
            if (rdBtnStudent.isChecked()) {
                etLoginNo.setHint("Student number");
                if (loginNo.length() == 9) {
                    if (password.length() >= 6) {
                        new loginTask("std",loginNo,password).execute();
                    } else {
                        Toast.makeText(MainActivity.this, "Password is too short", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(MainActivity.this, "Student number length should be equal to 9", Toast.LENGTH_LONG).show();
                }
            }
        } else {
            Toast.makeText(MainActivity.this, "Fill all the fields", Toast.LENGTH_LONG).show();
        }
    }

    private void setLink_signup()
    {
        link_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (rdBtnLecturer.isChecked()) {
                    Intent intent = new Intent(MainActivity.this, LRegister.class);
                    startActivity(intent);
                }
                if (rdBtnStudent.isChecked()){
                    Intent intent = new Intent(MainActivity.this, SRegister.class);
                    startActivity(intent);
                }
            }
        });
    }

    public class loginTask extends AsyncTask<String,String,String> {

        HttpURLConnection conn;
        URL url = null;
        String loginNo,password;
        String who;

        public loginTask(String who,String loginNo, String password) {

            this.loginNo = loginNo;
            this.password = password;
            this.who = who;
        }

        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Login...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... param) {

            try {

                Log.w("TAG"," who "+ who+" num "+ loginNo + " pass "+ password);
                // Enter URL address where your php file resides
                if(who.equals("lec")) {
                    url = new URL(urlCon);
                }else
                {
                    url = new URL(urlConne);
                }

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return "exception";
            }
            try {
                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection)url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);
                conn.setDoOutput(true);


                // Append parameters to URL
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("txtLoginNo", loginNo)
                        .appendQueryParameter("txtPassword", password);
                String query = builder.build().getEncodedQuery();

                // Open connection for sending data
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                return "exception";
            }

            try {

                int response_code = conn.getResponseCode();

                Log.d("response_code",""+ response_code);
                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    Log.d("data",""+ result.toString());
                    // Pass data to onPostExecute method
                    return(result.toString());

                }else{

                    return("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }


        }

        @Override
        protected void onPostExecute(String success) {

            pDialog.dismiss();
            if (success != null){
                Log.i("Website Content", success);
                try {
                    JSONObject jsonPart = new JSONObject(success);
                    int num = jsonPart.getInt(TAG_SUCCESS);
                    String message = jsonPart.getString(TAG_MESSAGE);

                    if (num == 1) {

                        if (rdBtnLecturer.isChecked()) {
                            Intent intent = new Intent(MainActivity.this, LecturerHome.class);
                            intent.putExtra("staff", loginNo);
                            startActivity(intent);
                        }else
                        {
                            Intent intent = new Intent(MainActivity.this, StudentHome.class);
                            intent.putExtra("stdnumber", loginNo);
                            startActivity(intent);
                        }


                    } else {
                        // pDialog.dismiss();
                        Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(MainActivity.this, "Permission denied - Accept permissions", Toast.LENGTH_SHORT).show();
                    finish();
                }

                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

}
