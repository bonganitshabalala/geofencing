package com.example.khumo.geofencing.serv;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.example.khumo.geofencing.R;
import com.example.khumo.geofencing.Student.ViewTimeTableActivity;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Bongani on 2017/10/06.
 */

public class GeofenceTransitionsIntentService extends IntentService {

    private static final String TAG = "GeofenceTransitions";
    private GoogleApiClient mGoogleApiClient;
    //private LocationRequest mLocationRequest;
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 10000;
    private static String urlcon = "http://petersdana98.000webhostapp.com/regAttendance.php";
    private static final String TAG_SUBJECT = "subject";
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_TIMESLOT = "timeslot";

    public GeofenceTransitionsIntentService() {
        super("GeofenceTransitionsIntentService");
    }


        @Override
        protected void onHandleIntent(Intent intent) {

            Log.i(TAG, "onHandleIntent");

            GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);
            if (geofencingEvent.hasError()) {
                //String errorMessage = GeofenceErrorMessages.getErrorString(this,
                //      geofencingEvent.getErrorCode());
                Log.e(TAG, "Goefencing Error " + geofencingEvent.getErrorCode());
                return;
            }
            String venue = intent.getStringExtra("venue");
            String studentnumber = intent.getStringExtra("studentnumber");
            String subject = intent.getStringExtra("subject");
            String start = intent.getStringExtra("start");
            String end = intent.getStringExtra("end");

            DateFormat dateFormat = new SimpleDateFormat("HH:mm");
            DateFormat dateFormat1 = new SimpleDateFormat("yyyy/MMMM/dd");
            Date date = new Date();
            String time = dateFormat.format(date);



            // Get the transition type.
            int geofenceTransition = geofencingEvent.getGeofenceTransition();

            Log.i(TAG, "geofenceTransition = " + geofenceTransition + " Enter : " + Geofence.GEOFENCE_TRANSITION_ENTER + "Exit : " + Geofence.GEOFENCE_TRANSITION_EXIT);
            if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER){
                showNotification("Entered", "Entered the Location");
                Log.i("TAG","name :  "+ venue + " sub "+ subject + " number "+studentnumber);
                storeDetails(venue,studentnumber,subject);
              // updateDetails details =  new updateDetails(venue,studentnumber,subject);
               // details.execute();
            }
            else if(geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) {
                Log.i(TAG, "Showing Notification...");
                showNotification("Exited", "Exited the Location");
            }
            else if(geofenceTransition == Geofence.GEOFENCE_TRANSITION_DWELL) {
                Log.i(TAG, "Showing Notification...");
                showNotification("Inside", "Inside Location");
            }else {
                // Log the error.
                showNotification("Error", "Error");
                Log.e(TAG, "Error ");
            }
        }

        public void showNotification(String text, String bigText) {

            // 1. Create a NotificationManager
            NotificationManager notificationManager =
                    (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

            // 2. Create a PendingIntent for AllGeofencesActivity
            Intent intent = new Intent(this, ViewTimeTableActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingNotificationIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

            // 3. Create and send a notification
            Notification notification = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.geo)
                    .setContentTitle("Geo")
                    .setContentText(text)
                    .setContentIntent(pendingNotificationIntent)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(bigText))
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setAutoCancel(true)
                    .build();
            notificationManager.notify(0, notification);
        }

    public void  storeDetails(String venue, String studentnumber, String subject) {

        HttpURLConnection conn = null;
        URL url = null;
        String lat,longi;


            try {

                // Enter URL address where your php file resides
                url = new URL(urlcon);

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            }
            try {
                DateFormat dateFormat = new SimpleDateFormat("HH:mm");
                DateFormat dateFormat1 = new SimpleDateFormat("yyyy/MM/dd");
                Date date = new Date();
                String time = dateFormat.format(date);
                String dates = dateFormat1.format(date);
                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection)url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);
                conn.setDoOutput(true);


                // Append parameters to URL
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("venue", venue)
                        .appendQueryParameter("studentnumber", studentnumber)
                        .appendQueryParameter("subject", subject)
                        .appendQueryParameter("date", dates)
                        .appendQueryParameter("time", time)
                        .appendQueryParameter("present", "yes");
                String query = builder.build().getEncodedQuery();

                // Open connection for sending data
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();

            }

            try {

                int response_code = conn.getResponseCode();

                Log.d("response_code",""+ response_code);
                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    Log.d("data",""+ result.toString());
                    // Pass data to onPostExecute method


                }else{


                }

            } catch (IOException e) {
                e.printStackTrace();

            } finally {
                conn.disconnect();
            }






    }
}
