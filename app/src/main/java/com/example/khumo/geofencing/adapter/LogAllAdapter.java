package com.example.khumo.geofencing.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.khumo.geofencing.R;
import com.example.khumo.geofencing.pojo.Attendance;
import com.example.khumo.geofencing.pojo.Report;

import java.util.List;

/**
 * Created by Bongani on 2017/11/15.
 */


public class LogAllAdapter extends ArrayAdapter<Report> {

    private Activity mContext = null;
    private LayoutInflater mInflater = null;

    private static class ViewHolder {

        private TextView tvStudent = null;
        private TextView tvName = null;
        private TextView tvSurname = null;
        private TextView tvVenue = null;
        private TextView tvSub = null;


    }

    public LogAllAdapter(Activity context, int textViewResourceId, List<Report> objects){
        super(context, textViewResourceId, objects);
        this.mContext = context;
        mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View contentView, final ViewGroup parent){
        final Report item = getItem(position);
        final ViewHolder vh;
        if(contentView == null){
            vh = new ViewHolder();
            contentView = mInflater.inflate(R.layout.report_logs, parent, false);


            vh.tvStudent = (TextView) contentView.findViewById(R.id.textViewStudentNr);
            vh.tvName = (TextView) contentView.findViewById(R.id.textViewName);
            vh.tvSurname = (TextView) contentView.findViewById(R.id.textViewSurname);
            vh.tvVenue = (TextView) contentView.findViewById(R.id.textViewVenue);
            vh.tvSub = (TextView) contentView.findViewById(R.id.textViewSub);



            contentView.setTag(vh);
        } else {
            vh = (ViewHolder) contentView.getTag();
        }
        vh.tvStudent.setText(item.getStudentNumber());
        vh.tvName.setText(item.getName());
        vh.tvSurname.setText(item.getSurname());
        vh.tvVenue.setText(item.getVenue());
        vh.tvSub.setText(item.getSubject());



        return (contentView);
    }
}