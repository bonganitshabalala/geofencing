package com.example.khumo.geofencing.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.example.khumo.geofencing.Lecturer.LecturerHome;
import com.example.khumo.geofencing.Lecturer.ViewActivity;
import com.example.khumo.geofencing.R;
import com.example.khumo.geofencing.pojo.Lecture;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bongani on 2017/10/16.
 */

public class SubjectDialogFragment extends DialogFragment {

    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 10000;
    private static String urlConne = "http://petersdana98.000webhostapp.com/addSubject.php";
    private static String urlz = "http://app-1505651429.000webhostapp.com/getContact.php";
    private static final String TAG_USER = "con";
    private static String phoneNumber;
    private EditText etCode,etSubject;
    private String code,subject;
    Activity activity;
    private ProgressDialog pDialog;
    static String pass;
    static String staffnumber;




    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";

    public static SubjectDialogFragment newInstance(String staff) {

        Bundle bundle = new Bundle();
        staffnumber = staff;
        SubjectDialogFragment sampleFragment = new SubjectDialogFragment();
        sampleFragment.setArguments(bundle);
        return sampleFragment;
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.add_subject, null);
            Toolbar toolbar = (Toolbar) dialogView.findViewById(R.id.toolbar);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                toolbar.setTitle("Add Subject");
            }
            //updateView(dialogView);
            final Bundle args = getArguments();
            Log.w("TAG", "In here");

            etCode = (EditText) dialogView.findViewById(R.id.code);
            etSubject = (EditText) dialogView.findViewById(R.id.subjectname);





            final AlertDialog alertDialog = new AlertDialog.Builder(getActivity(), R.style.MyWhiteAlertDialogStyle)
                    .setView(dialogView)
                    .setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .setPositiveButton("Add", null)
                    .create();
            alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(final DialogInterface dialog) {
                    Button b = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
                    b.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            attemptRegistration();


                        }
                    });
                }
            });
            return alertDialog;
        } else {
            return super.onCreateDialog(savedInstanceState);
        }
    }

    private void attemptRegistration() {

        // Store values at the time of the login attempt.

        code = etCode.getText().toString();

        subject = etSubject.getText().toString();


        boolean cancel = false;
        View focusView = null;


        if (TextUtils.isEmpty(code)) {
            etCode.setError("Error - code can't be empty");
            focusView = etCode;
            cancel = true;

        }

        if (TextUtils.isEmpty(subject)) {
            etSubject.setError("Error - subject can't be empty");
            focusView = etSubject;
            cancel = true;

        }



//        if (!TextUtils.isEmpty(code)) {
//            if(!code.matches("[a-zA-Z]+")) {
//                etCode.setError("Error - code should not contain spaces");
//                focusView = etCode;
//                cancel = true;
//            }
//
//        }




        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            if (!isOnline(getActivity())) {
                Toast.makeText(getActivity(), "No internet connection", Toast.LENGTH_LONG).show();
            } else {

                new AddContactTask().execute();



            }
        }
    }

    private boolean isOnline(Context mContext) {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    public class AddContactTask extends AsyncTask<String,String,String> {

        HttpURLConnection conn;
        URL url = null;
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Adding contact...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... param) {

            try {

                // Enter URL address where your php file resides
                url = new URL(urlConne);

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return "exception";
            }
            try {
                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection)url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);
                conn.setDoOutput(true);

                // Append parameters to URL
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("code", code)
                        .appendQueryParameter("subject", subject)
                        .appendQueryParameter("staffnumber", staffnumber);
                String query = builder.build().getEncodedQuery();

                // Open connection for sending data
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                return "exception";
            }

            try {

                int response_code = conn.getResponseCode();

                Log.d("response_code",""+ response_code);
                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    Log.d("data",""+ result.toString());
                    // Pass data to onPostExecute method
                    return(result.toString());

                }else{

                    return("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }


        }

        @Override
        protected void onPostExecute(String success) {

            pDialog.dismiss();
            if (success != null){
                Log.i("Website Content", success);
                try {
                    JSONObject jsonPart = new JSONObject(success);
                    int num = jsonPart.getInt(TAG_SUCCESS);
                    String message = jsonPart.getString(TAG_MESSAGE);

                    if (num == 1) {


                        Intent intent = new Intent(getActivity(),ViewActivity.class);
                        intent.putExtra("staff",staffnumber);
                        startActivity(intent);
                        Toast.makeText(getActivity(), "Subject added successfully", Toast.LENGTH_LONG).show();


                    } else {
                        // pDialog.dismiss();
                        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }

}
