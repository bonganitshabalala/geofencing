package com.example.khumo.geofencing.Student;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.khumo.geofencing.Lecturer.ViewActivity;
import com.example.khumo.geofencing.Lecturer.ViewStudentAttendance;
import com.example.khumo.geofencing.R;
import com.example.khumo.geofencing.pojo.Attendance;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Bongani on 2017/10/30.
 */

public class ViewStudentReportDialogFragment extends DialogFragment {


   // List<Attend> attendanceList = new ArrayList<>();
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 10000;
    private static String urlcon = "http://petersdana98.000webhostapp.com/getAtten.php";

    private static final String TAG_MESSAGE = "message";
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_TIMESLOT = "att";
    private ProgressDialog pDialog;
    private static String staffNum;
    private static String subject,studentnumber;
    TextView txtResults;


    public static ViewStudentReportDialogFragment newInstance(String staff,String subj) {

        Bundle bundle = new Bundle();
        staffNum = staff;
        subject = subj;
        ViewStudentReportDialogFragment sampleFragment = new ViewStudentReportDialogFragment();
        sampleFragment.setArguments(bundle);
        return sampleFragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.report_view, null);
            Toolbar toolbar = (Toolbar) dialogView.findViewById(R.id.toolbar);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                toolbar.setTitle("Results");
            }
            //updateView(dialogView);
            final Bundle args = getArguments();

            txtResults = (TextView) dialogView.findViewById(R.id.textView8);
          //  Log.w("TAG", "In here "+ staffnumber + " ss "+ subject);

           // listView = (ListView)dialogView.findViewById(R.id.listView);


            new contactTask(staffNum,subject).execute();

//            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                @Override
//                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//
//
//                    Intent intent = new Intent(getActivity(), ViewStudentAttendance.class);
//                    intent.putExtra("studentnumber",studentnumber);
//                    intent.putExtra("subject",subject);
//                    intent.putExtra("staff",staffnumber);
//                    getActivity().startActivity(intent);
//
//
//
//                }
//            });



            final AlertDialog alertDialog = new AlertDialog.Builder(getActivity(), R.style.MyWhiteAlertDialogStyle)
                    .setView(dialogView)
                    .setPositiveButton("Cancel", null)
                    .create();
            alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(final DialogInterface dialog) {
                    Button b = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
                    b.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            // attemptRegistration();
                            dialog.dismiss();
//                            Intent i = new Intent(getActivity(),ViewActivity.class);
//                            i.putExtra("staff",staffNum);
//                            startActivity(i);


                        }
                    });
                }
            });
            return alertDialog;
        } else {
            return super.onCreateDialog(savedInstanceState);
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }



    public class contactTask extends AsyncTask<String,String,String> {

        HttpURLConnection conn;
        URL url = null;
        String studentnumber,subject;

        public contactTask(String studentnumber,String subject) {

            this.studentnumber = studentnumber;
            this.subject = subject;
        }

        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Getting report...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... param) {

            try {

                // Enter URL address where your php file resides
                url = new URL(urlcon);

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return "exception";
            }
            try {
                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection)url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);
                conn.setDoOutput(true);

                // Append parameters to URL
                Log.w("TAG ","s "+ studentnumber + " ssa "+ subject);
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("studentnumber", studentnumber)
                        .appendQueryParameter("subject",subject);
                String query = builder.build().getEncodedQuery();

                // Open connection for sending data
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                return "exception";
            }

            try {

                int response_code = conn.getResponseCode();

                Log.d("response_code",""+ response_code);
                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    Log.d("data",""+ result.toString());
                    // Pass data to onPostExecute method
                    return(result.toString());

                }else{

                    return("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }


        }

        @Override
        protected void onPostExecute(String success) {

            pDialog.dismiss();
            if (success != null){
                Log.i("Website Content", success);
                try {
                    JSONObject jsonPart = new JSONObject(success);
                    int num = jsonPart.getInt(TAG_SUCCESS);
                    JSONArray jsonArray = jsonPart.getJSONArray(TAG_TIMESLOT);

                    if (num == 1) {

                            JSONObject jsonObject = jsonArray.getJSONObject(0);

                            //setAddresList(address);

                            String percentage = jsonObject.getString("percentage");

                        if(percentage != null || !percentage.equals("")) {

                            int perc = Integer.parseInt(percentage.substring(0, percentage.indexOf(",")));
                            if (perc >= 50) {
                                txtResults.setText("Your attendance percentage is : " + perc + "% \n Good luck and study hard \n You have gained entry to write an exam");
                            } else {
                                txtResults.setText("Your attendance percentage is : " + perc + "% \n You are not serious about this subject \n You wont be writing an exam.");
                            }
                        }else
                        {
                            txtResults.setText("Your attendance has not yet been recorded, contact your lecturer");
                        }








                      //  display(attendanceList);



                        //new loadAccount().execute();

                    } else {

                        Toast.makeText(getActivity(), "No reports available for this student", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }

}
