package com.example.khumo.geofencing.Student;

import android.Manifest;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Rect;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.khumo.geofencing.FinishedActivity;
import com.example.khumo.geofencing.R;
import com.example.khumo.geofencing.adapter.TokensAdapter;
import com.example.khumo.geofencing.pojo.Timeslot;
import com.example.khumo.geofencing.pojo.Token;
import com.example.khumo.geofencing.serv.GeofenceTransitionsIntentService;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by Bongani on 2017/10/19.
 */

public class ViewTimeTableActivity extends AppCompatActivity implements LocationListener
{


    List<Timeslot> timeslotList = new ArrayList<>();
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 10000;
    private static String urlcon = "http://petersdana98.000webhostapp.com/getStudentTimetable.php";
    private static String urlconne = "http://petersdana98.000webhostapp.com/regAttendance.php";
    private static final String TAG_SUBJECT = "subject";
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_TIMESLOT = "timeslot";
    private ProgressDialog pDialog;
    private String stuNumber,subject,strvenue,starttime,endtime;

    private List<Token> tokenList = new ArrayList<>();

    private RecyclerView recyclerView;

    TokensAdapter adapter;

   // double currentLatitude = -33.9791415, currentLongitude = 18.7709281;

    PendingIntent mGeofencePendingIntent;
    public static final int CONNECTION_FAILURE_RESOLUTION_REQUEST = 100;
    private List<Geofence> mGeofenceList;
    private GoogleApiClient mGoogleApiClient;
    public static final String TAG = "Activity";
    LocationRequest mLocationRequest;
    double currentLatitude , currentLongitude ;
    Boolean locationFound;
    protected LocationManager locationManager;
    protected LocationListener locationListener;
    String strVenue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.summary_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        stuNumber = getIntent().getStringExtra("stdnumber");
        subject = getIntent().getStringExtra("subject");

        initCollapsingToolbar();

        recyclerView = (RecyclerView)findViewById(R.id.recycler_view);

        mGeofenceList = new ArrayList<Geofence>();

        new contactTask(stuNumber,subject).execute();

        TokensAdapter.OnClickListener adapterClicked = new TokensAdapter.OnClickListener() {
            @Override
            public void onClick(Token token) {

//                    PurchaseDialogFragment logOptionsDialogFragment = PurchaseDialogFragment.newInstance(token,toke,email );
//                    android.support.v4.app.FragmentTransaction ft = getFragmentManager().beginTransaction();
//                    ft.add(logOptionsDialogFragment,"newFragment");
//                    ft.commit();

            }
        };





        adapter = new TokensAdapter(this, timeslotList,adapterClicked);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 3);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(3, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        int resp = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resp == ConnectionResult.SUCCESS) {

            initGoogleAPIClient();




        } else {
            Toast.makeText(ViewTimeTableActivity.this, "Your Device doesn't support Google Play Services.",Toast.LENGTH_SHORT).show();
        }

        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(1 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000); // 1 second, in milliseconds

        //prepareTokens();

        Button btnBack = (Button)findViewById(R.id.buttonBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent supdate = new Intent(ViewTimeTableActivity.this, SViewActivity.class);
                supdate.putExtra("stdnumber",stuNumber);
                startActivity(supdate);
            }
        });

    }

        @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.timetable_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.ic_report) {

            ViewStudentReportDialogFragment logOptionsDialogFragment = ViewStudentReportDialogFragment.newInstance(stuNumber,subject);
            android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.add(logOptionsDialogFragment,"newFragment");
            ft.commit();

            return true;
        }else  if (id == R.id.ic_web) {

            Intent i = new Intent(Intent.ACTION_VIEW,Uri.parse("https://ipenabler.tut.ac.za/pls/prodi03/w99pkg.mi_login"));
            startActivity(i);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void initGoogleAPIClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(connectionAddListener)
                .addOnConnectionFailedListener(connectionFailedListener)
                .build();
        mGoogleApiClient.connect();
    }

    private GoogleApiClient.ConnectionCallbacks connectionAddListener =
            new GoogleApiClient.ConnectionCallbacks() {
                @Override
                public void onConnected(Bundle bundle) {
                    Log.i(TAG, "onConnected");

                    if (ActivityCompat.checkSelfPermission(ViewTimeTableActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ViewTimeTableActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

                    if (location == null) {
                        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, ViewTimeTableActivity.this);

                    } else {
                        //If everything went fine lets get latitude and longitude
                        currentLatitude = location.getLatitude();
                        currentLongitude = location.getLongitude();

                        Log.i(TAG, currentLatitude + " WORKS " + currentLongitude);

                        //createGeofences(currentLatitude, currentLongitude);
                        //registerGeofences(mGeofenceList);
                        createGeofences(currentLatitude, currentLongitude);;

                        Log.i(TAG, currentLatitude + " WORKS " + currentLongitude);

                        //createGeofences(currentLatitude, currentLongitude);
                        //registerGeofences(mGeofenceList);
                       // createGeofences(currentLatitude, currentLongitude);

                    }


                }

                @Override
                public void onConnectionSuspended(int i) {

                    Log.e(TAG, "onConnectionSuspended");

                }
            };

    private GoogleApiClient.OnConnectionFailedListener connectionFailedListener =
            new GoogleApiClient.OnConnectionFailedListener() {
                @Override
                public void onConnectionFailed(ConnectionResult connectionResult) {
                    Log.e(TAG, "onConnectionFailed");
                }
            };

    /**
     * Create a Geofence list
     */
    public void createGeofences(double latitude, double longitude) {
        try {
            String id = UUID.randomUUID().toString();

           // for(Geofence geofence : mGeofenceList)
          //  {
               // if(!geofence.getRequestId().equals(id))
                //{
                    SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(ViewTimeTableActivity.this);

//                    SharedPreferences.Editor editor = mPrefs.edit();
//                    editor.putString("venue", venue);
//                    editor.putString("studentnumber", stuNumber);
//                    editor.putString("subject", subject);
//                    new updateDetails(venue,studentnumber,subject).execute();
                    //editor.commit();
                    Geofence fence = new Geofence.Builder()
                            .setRequestId(id)
                            .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT)
                            .setCircularRegion(latitude, longitude, 50)
                            .setExpirationDuration(Geofence.NEVER_EXPIRE)
                            .build();
                    mGeofenceList.add(fence);

                //}
          //  }

            Log.w("TAG"," created");


                 }catch (Exception x)
        {

        }
        //Circle circle =

    }

    private GeofencingRequest getGeofencingRequest() {

        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);
        builder.addGeofences(mGeofenceList);
        return builder.build();
    }

    private PendingIntent getGeofencePendingIntent() {
        // Reuse the PendingIntent if we already have it.
        if (mGeofencePendingIntent != null) {
            Log.i(TAG, "FLAG_UPDATE_CURRENT");
            return mGeofencePendingIntent;
        }
        Intent intent = new Intent(this, GeofenceTransitionsIntentService.class);
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when
        // calling addGeofences() and removeGeofences().

        intent.putExtra("venue",strvenue);
        intent.putExtra("studentnumber",stuNumber);
        intent.putExtra("subject",subject);
        intent.putExtra("start",starttime);
        intent.putExtra("end",endtime);

        return PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    @Override
    public void onLocationChanged(Location location) {
        currentLatitude = location.getLatitude();
        currentLongitude = location.getLongitude();
        Log.i(TAG, "onLocationChanged");
    }
    /**
     * Initializing collapsing toolbar
     * Will show and hide the toolbar title on scroll
     */
    private void initCollapsingToolbar() {
        final CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle(" ");
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appbar);
        appBarLayout.setExpanded(true);

        // hiding & showing the title when toolbar expanded & collapsed
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbar.setTitle(getString(R.string.app_name));
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbar.setTitle(" ");
                    isShow = false;
                }
            }
        });
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    public class updateDetails extends AsyncTask<String,String,String> {

        HttpURLConnection conn;
        URL url = null;
        String venue,studentnumber,subject,strEmail;
        String lat,longi;

        public updateDetails(String venue, String studentnumber, String subject) {

            this.venue = venue;
            this.studentnumber = studentnumber;
            this.subject = subject;

        }

        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... param) {

            try {

                // Enter URL address where your php file resides
                url = new URL(urlconne);

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return "exception";
            }
            try {
                DateFormat dateFormat = new SimpleDateFormat("HH:mm");
                DateFormat dateFormat1 = new SimpleDateFormat("yyyy/MMMM/dd");
                Date date = new Date();
                String time = dateFormat.format(date);
                String dates = dateFormat1.format(date);
                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection)url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);
                conn.setDoOutput(true);


                // Append parameters to URL
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("venue", venue)
                        .appendQueryParameter("studentnumber", studentnumber)
                        .appendQueryParameter("subject", subject)
                        .appendQueryParameter("date", dates)
                        .appendQueryParameter("time", time)
                        .appendQueryParameter("present", "Yes");
                String query = builder.build().getEncodedQuery();

                // Open connection for sending data
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                return "exception";
            }

            try {

                int response_code = conn.getResponseCode();

                Log.d("response_code",""+ response_code);
                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    Log.d("data",""+ result.toString());
                    // Pass data to onPostExecute method
                    return(result.toString());

                }else{

                    return("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }


        }

        @Override
        protected void onPostExecute(String success) {

            if (success != null){
                Log.i("Website Content", success);

            }
        }

    }



    public class contactTask extends AsyncTask<String,String,String> {

        HttpURLConnection conn;
        URL url = null;
        String studentnumber,subject;

        public contactTask(String number,String subject) {

            studentnumber = number;
            this.subject = subject;
        }

        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(ViewTimeTableActivity.this);
            pDialog.setMessage("Getting timeslot...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... param) {

            try {

                // Enter URL address where your php file resides
                url = new URL(urlcon);

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return "exception";
            }
            try {
                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection)url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);
                conn.setDoOutput(true);

                // Append parameters to URL
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("studentnumber", studentnumber)
                        .appendQueryParameter("subject",subject);
                String query = builder.build().getEncodedQuery();

                // Open connection for sending data
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                return "exception";
            }

            try {

                int response_code = conn.getResponseCode();

                Log.d("response_code",""+ response_code);
                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    Log.d("data",""+ result.toString());
                    // Pass data to onPostExecute method
                    return(result.toString());

                }else{

                    return("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }


        }

        @Override
        protected void onPostExecute(String success) {

            pDialog.dismiss();
            if (success != null){
                Log.i("Website Content", success);
                try {
                    JSONObject jsonPart = new JSONObject(success);
                    int num = jsonPart.getInt(TAG_SUCCESS);
                    JSONArray jsonArray = jsonPart.getJSONArray(TAG_TIMESLOT);

                    if (num == 1) {

                        for(int x =0 ; x < jsonArray.length(); x++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(x);

                            //setAddresList(address);

                            strvenue = jsonObject.getString("venue");
                            String day = jsonObject.getString("day");
                            starttime = jsonObject.getString("start");
                             endtime = jsonObject.getString("end");
                            String lati = jsonObject.getString("lati");
                            String longi = jsonObject.getString("longi");

                            Timeslot myContact = new Timeslot(strvenue,day,starttime,endtime,lati,longi);
                            if(myContact != null)
                            {
                                if (!timeslotList.contains(myContact)) {
                                    timeslotList.add(myContact);
                                }

                            }




                        }

                        for( Timeslot timeslot : timeslotList)
                        {

                            currentLatitude = Double.parseDouble(timeslot.getLatitude());
                            currentLongitude = Double.parseDouble(timeslot.getLongitude());
                            Log.i("timeslot","currentLatitude "+currentLatitude+" currentLongitude "+currentLongitude);
                            createGeofences(currentLatitude, currentLongitude); //,timeslot.getVenue()
                        }
                        try{
                            if (ActivityCompat.checkSelfPermission(ViewTimeTableActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ViewTimeTableActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                // TODO: Consider calling
                                //    ActivityCompat#requestPermissions
                                // here to request the missing permissions, and then overriding
                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                //                                          int[] grantResults)
                                // to handle the case where the user grants the permission. See the documentation
                                // for ActivityCompat#requestPermissions for more details.
                                return;
                            }
                            LocationServices.GeofencingApi.addGeofences(
                                    mGoogleApiClient,
                                    getGeofencingRequest(),
                                    getGeofencePendingIntent()
                            );

                        } catch (Exception securityException) {
                            // Catch exception generated if the app does not use ACCESS_FINE_LOCATION permission.
                            Log.e(TAG, "Error");
                        }

                        TokensAdapter.OnClickListener adapterClicked = new TokensAdapter.OnClickListener() {
                            @Override
                            public void onClick(Token token) {

//                    PurchaseDialogFragment logOptionsDialogFragment = PurchaseDialogFragment.newInstance(token,toke,email );
//                    android.support.v4.app.FragmentTransaction ft = getFragmentManager().beginTransaction();
//                    ft.add(logOptionsDialogFragment,"newFragment");
//                    ft.commit();

                            }
                        };

                        adapter = new TokensAdapter(ViewTimeTableActivity.this, timeslotList,adapterClicked);

                        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(ViewTimeTableActivity.this, 3);
                        recyclerView.setLayoutManager(mLayoutManager);
                        recyclerView.addItemDecoration(new GridSpacingItemDecoration(3, dpToPx(10), true));
                        recyclerView.setItemAnimator(new DefaultItemAnimator());
                        recyclerView.setAdapter(adapter);


                    } else {

                        Toast.makeText(ViewTimeTableActivity.this, "Could not load subjects", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }


}
