package com.example.khumo.geofencing.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.khumo.geofencing.R;
import com.example.khumo.geofencing.pojo.Subject;

import java.util.List;

/**
 * Created by Bongani on 2017/10/18.
 */


public class TimeslotAdapter extends ArrayAdapter<Subject> {

    private Activity mContext = null;
    private LayoutInflater mInflater = null;
    List<Subject> subjects ;

    private static class ViewHolder {
        private TextView mSideBarText = null;
        private TextView mSideBar = null;
    }

    public TimeslotAdapter(Activity context, int textViewResourceId, List<Subject> objects){
        super(context, textViewResourceId, objects);
        this.mContext = context;
        subjects = objects;
        mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public View getView(int position, View contentView, final ViewGroup parent){
        final ViewHolder vh;
        final Subject subject = subjects.get(position);
        if(contentView == null){
            vh = new ViewHolder();
            contentView = mInflater.inflate(R.layout.sidebar_cart, parent, false);

            vh.mSideBarText = (TextView) contentView.findViewById(R.id.sideBarText);
            vh.mSideBar = (TextView) contentView.findViewById(R.id.sideBar);
            contentView.setTag(vh);
        } else {
            vh = (ViewHolder) contentView.getTag();
        }

        vh.mSideBar.setText(" "+ subject.getSubject());

        return (contentView);
    }

    @Override
    public View getDropDownView(int position, View contentView, final ViewGroup parent){
        final ViewHolder vh;
        final Subject subject = subjects.get(position);
        if(contentView == null){
            vh = new ViewHolder();
            contentView = mInflater.inflate(R.layout.sidebar_cart, parent, false);

            vh.mSideBarText = (TextView) contentView.findViewById(R.id.sideBarText);
            vh.mSideBar = (TextView) contentView.findViewById(R.id.sideBar);
            contentView.setTag(vh);
        } else {
            vh = (ViewHolder) contentView.getTag();
        }

        vh.mSideBar.setText(" "+ subject.getSubject());

        return (contentView);
    }


}