package com.example.khumo.geofencing;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Bongani on 2016/12/01.
 */
public class LogEntry {
    public String tag;
    public Date timeStamp;
    public String message;

    public LogEntry(String Tag, String Message, Date TimeStamp){
        tag = Tag;
        message = Message;
        timeStamp = TimeStamp;
    }

    public String getTime(){
        //DateFormat dateformat = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.LONG);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        //dateFormat.setTimeZone(TimeZone.getDefault());
        String timeString = timeStamp != null ? dateFormat.format(this.timeStamp) : "N/A";
        return timeString;
    }

    public String toString(){
        return "["+getTime()+"] : "+tag+" : "+message;
    }
}
